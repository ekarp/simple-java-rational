Simple Java Rational Library
============================

Simple Java Rational is a lightweight, arbitrary precision, rational arithmetic library, with a MIT license. I was inspired to write it after observing that Apache’s and JScience’s implementations are packaged in jars that include ~700kb to ~2mb of classes (as of December 2015), which unnecessarily added bulk to the program I was writing.

This version compiles to a 8kb class file or a 5kb jar file.

Please see the [home page](https://ekarp.com/projects/simple-java-rational/) for more information.

## Example

~~~~
import com.ekarp.math.Rational;
import java.math.BigDecimal;
import java.math.BigInteger;

public class Example {

    public static void main(String[] args) {
        // Constants 0 and 1 are pre-defined.
        System.out.println(Rational.ZERO.toString());  // Prints 0.
        System.out.println(Rational.ONE.toString());  // Prints 1.

        // Define rationals using big integers.
        Rational threeQuarters = new Rational(new BigInteger("3"),
                                              new BigInteger("4"));
        System.out.println(threeQuarters.toString()); // Prints 3/4.

        // Has constructors for longs, strings, and BigDecimals.
        threeQuarters = new Rational(3, 4);
        System.out.println(threeQuarters.toString()); // Prints 3/4.

        threeQuarters =  new Rational("3/4");
        System.out.println(threeQuarters.toString()); // Prints 3/4.

        threeQuarters = new Rational(new BigDecimal("0.75"));
        System.out.println(threeQuarters.toString()); // Prints 3/4.

        // Rationals with a divisor of 1 are printed without the denominator.
        Rational reduced = new Rational(1000, 1);
        System.out.println(reduced.toString()); // Prints 1000.

        // Common factors are removed.
        Rational factored = new Rational(120, 240);
        System.out.println(factored.toString()); // Prints 1/2.

        // Standard arithmetic is available.
        Rational r = threeQuarters.subtract(reduced).multiply(factored).add(
            threeQuarters); // ((3/4 - 1000) * 1/2) + 3/4 = -3991/8.
        // Prints -3991, the numerator always carries the sign.
        System.out.printf("Numerator: %s\n", r.numerator());
        // Prints 8.
        System.out.printf("Denominator: %s\n", r.denominator());
    }
}
~~~~