/*
The MIT License (MIT)

Copyright (c) 2015 Elliott Karpilovsky

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package com.ekarp.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class RationalTest {

    /**
     * A list of rational numbers to use for tests.
     */
    private static final List<Rational> TEST_RATIONALS;
    static {
        List<Rational> result = new ArrayList<>();
        for (int i = -20; i < 20; ++i) {
            for (int j = 1; j < 20; ++j) {
                BigInteger n = new BigInteger(String.valueOf(i));
                BigInteger d = new BigInteger(String.valueOf(j));
                result.add(new Rational(n, d));
            }
        }
        TEST_RATIONALS = Collections.unmodifiableList(result);
    }

    /**
     * The set of test rational numbers must have at least 100 positive values
     * and 100 negative values.
     */
    @Test
    public void checkTestRationals() {
        Set<Rational> positive = new HashSet<>();
        Set<Rational> negative = new HashSet<>();
        for (Rational r: TEST_RATIONALS) {
            if (r.compareTo(Rational.ZERO) < 0) {
                negative.add(r);
            } else if (r.compareTo(Rational.ZERO) > 0) {
                positive.add(r);
            }
        }
        Assert.assertTrue(positive.size() >= 100);
        Assert.assertTrue(negative.size() >= 100);
    }

    /**
     * The test rational numbers must include zero.
     */
    @Test
    public void zeroInTestRationals() {
        for (Rational r: TEST_RATIONALS) {
            if (r.equals(Rational.ZERO)) {
                return;
            }
        }
        Assert.fail("Zero not in test rationals.");
    }

    /**
     * Test big integer constructor and getters.
     */
    @Test
    public void bigIntConstructAndGetters() {
        Rational r = new Rational(new BigInteger("113"), new BigInteger("200"));
        Assert.assertEquals(new BigInteger("113"), r.numerator());
        Assert.assertEquals(new BigInteger("200"), r.denominator());
        r = new Rational(new BigInteger("113"));
        Assert.assertEquals(new BigInteger("113"), r.numerator());
        Assert.assertEquals(BigInteger.ONE, r.denominator());
    }

    /**
     * Test big decimal constructor and getters.
     */
    @Test
    public void bigDecConstructAndGetters() {
        Rational r = new Rational(new BigDecimal(".565"));
        Assert.assertEquals(new BigInteger("113"), r.numerator());
        Assert.assertEquals(new BigInteger("200"), r.denominator());
    }

    /**
     * Test string constructor and getters.
     */
    @Test
    public void stringConstructAndGetters() {
        Rational r = new Rational("3.5");
        Assert.assertEquals(new BigInteger("7"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational("10/3");
        Assert.assertEquals(new BigInteger("10"), r.numerator());
        Assert.assertEquals(new BigInteger("3"), r.denominator());
    }

    /**
     * Test long constructor and getters.
     */
    @Test
    public void longConstructAndGetters() {
        Rational r = new Rational(113, 200);
        Assert.assertEquals(new BigInteger("113"), r.numerator());
        Assert.assertEquals(new BigInteger("200"), r.denominator());
        r = new Rational(113);
        Assert.assertEquals(new BigInteger("113"), r.numerator());
        Assert.assertEquals(BigInteger.ONE, r.denominator());
    }

    /**
     * Denominator cannot be zero.
     */
    @Test(expected=ArithmeticException.class)
    public void bigIntZeroDenominator() {
        new Rational(BigInteger.ONE, BigInteger.ZERO);
    }

    @Test(expected=ArithmeticException.class)
    public void stringZeroDenominator() {
        new Rational("1/0");
    }

    @Test(expected=ArithmeticException.class)
    public void longZeroDenominator() {
        new Rational(1, 0);
    }

    /**
     * The numerator always carries the sign.
     */
    @Test
    public void bigIntNumeratorSigned() {
        Rational r = new Rational(BigInteger.ONE, new BigInteger("2"));
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(new BigInteger("-1"), new BigInteger("2"));
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(BigInteger.ONE, new BigInteger("-2"));
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(new BigInteger("-1"), new BigInteger("-2"));
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());
    }

    @Test
    public void bigDecNumeratorSigned() {
        Rational r = new Rational(new BigDecimal("0.5"));
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(new BigDecimal("-0.5"));
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());
    }

    @Test
    public void stringNumeratorSigned() {
        Rational r = new Rational(1, 2);
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(-1, 2);
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(1, -2);
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(-1, -2);
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());
    }

    @Test
    public void longNumeratorSigned() {
        Rational r = new Rational(1, 2);
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(-1, 2);
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(1, -2);
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(-1, -2);
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());
    }

    /**
     * The rational is always in its reduced form.
     */
    @Test
    public void bigIntReducedForm() {
        Rational r = new Rational(new BigInteger("2"), new BigInteger("4"));
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(new BigInteger("-100"), new BigInteger("200"));
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(new BigInteger("10000"), new BigInteger("-20000"));
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(new BigInteger("-200"), new BigInteger("-400"));
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());
    }

    @Test
    public void bigDecReducedForm() {
        Rational r = new Rational(new BigDecimal("0.5"));
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(new BigDecimal("-0.500000000000000000000000000000"));
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());
    }

    @Test
    public void stringReducedForm() {
        Rational r = new Rational("2/4");
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational("-100/200");
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational("10000/-20000");
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational("-200/-400");
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());
    }

    @Test
    public void longReducedForm() {
        Rational r = new Rational(2, 4);
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(-100, 200);
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(10000, -20000);
        Assert.assertEquals(new BigInteger("-1"), r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());

        r = new Rational(-200, -400);
        Assert.assertEquals(BigInteger.ONE, r.numerator());
        Assert.assertEquals(new BigInteger("2"), r.denominator());
    }

    /**
     * Test addition.
     */
    @Test
    public void addition() {
        Rational r1 = new Rational(1, 2);
        Rational r2 = new Rational(1, 2);
        Rational r3 = new Rational(-1, 2);
        Assert.assertEquals(Rational.ONE, r1.add(r2));
        Assert.assertEquals(Rational.ZERO, r1.add(r3));
        Assert.assertEquals(Rational.ZERO, r3.add(r1));

        Rational r4 = new Rational(2, 3);
        Assert.assertEquals(new Rational(7, 6), r1.add(r4));
        Assert.assertEquals(new Rational(7, 6), r4.add(r1));

        // Add a big integer value.
        Assert.assertEquals(new Rational(3, 2), r1.add(BigInteger.ONE));
        Assert.assertEquals(r1.negate(),
                            r1.add(BigInteger.ONE.negate()));
        Assert.assertEquals(r1, r1.add(BigInteger.ZERO));
        Assert.assertEquals(new Rational(5, 2), r1.add(new BigInteger("2")));
        Assert.assertEquals(new Rational(-3, 2), r1.add(new BigInteger("-2")));

        // Add a big decimal value.
        Assert.assertEquals(new Rational(3, 2), r1.add(BigDecimal.ONE));
        Assert.assertEquals(r1.negate(),
                            r1.add(BigDecimal.ONE.negate()));
        Assert.assertEquals(r1, r1.add(BigDecimal.ZERO));
        Assert.assertEquals(new Rational(5, 2), r1.add(new BigDecimal("2")));
        Assert.assertEquals(new Rational(-3, 2), r1.add(new BigDecimal("-2")));
        Assert.assertEquals(new Rational(-2),
                            r1.add(new BigDecimal("-2.5")));

        // Add a long value.
        Assert.assertEquals(new Rational(3, 2), r1.add(1));
        Assert.assertEquals(new Rational(-1, 2), r1.add(-1));
        Assert.assertEquals(r1, r1.add(0));
        Assert.assertEquals(new Rational(5, 2), r1.add(2));
        Assert.assertEquals(new Rational(-3, 2), r1.add(-2));

        // Try many different whole numbers.
        for (int i = 0; i < 30; ++i) {
            Rational n = new Rational(i);
            Assert.assertEquals(r1.add(n), r1.add(i));
            Assert.assertEquals(r1.add(n), r1.add(new BigDecimal(i)));
            Assert.assertEquals(r1.add(n), r1.add(BigInteger.valueOf(i)));
        }
    }

    /**
     * Test subtraction.
     */
    @Test
    public void subtraction() {
        Rational r1 = new Rational(1, 2);
        Rational r2 = new Rational(1, 2);
        Rational r3 = new Rational(-1, 2);
        Assert.assertEquals(Rational.ZERO, r1.subtract(r2));
        Assert.assertEquals(Rational.ONE, r1.subtract(r3));
        Assert.assertEquals(new Rational(-1, 1), r3.subtract(r1));

        Rational r4 = new Rational(2, 3);
        Assert.assertEquals(new Rational(-1, 6), r1.subtract(r4));
        Assert.assertEquals(new Rational(1, 6), r4.subtract(r1));

        // Subtract a big integer value.
        Assert.assertEquals(new Rational(-1, 2), r1.subtract(BigInteger.ONE));
        Assert.assertEquals(new Rational(3, 2),
                            r1.subtract(BigInteger.ONE.negate()));
        Assert.assertEquals(r1, r1.subtract(BigInteger.ZERO));
        Assert.assertEquals(new Rational(-3, 2),
                            r1.subtract(new BigInteger("2")));
        Assert.assertEquals(new Rational(5, 2),
                            r1.subtract(new BigInteger("-2")));

        // Subtract a big decimal value.
        Assert.assertEquals(new Rational(-1, 2), r1.subtract(BigDecimal.ONE));
        Assert.assertEquals(new Rational(3, 2),
                            r1.subtract(BigDecimal.ONE.negate()));
        Assert.assertEquals(r1, r1.subtract(BigDecimal.ZERO));
        Assert.assertEquals(new Rational(-3, 2),
                            r1.subtract(new BigDecimal("2")));
        Assert.assertEquals(new Rational(5, 2),
                            r1.subtract(new BigDecimal("-2")));
        Assert.assertEquals(new Rational(3),
                            r1.subtract(new BigDecimal("-2.5")));

        // Subtract a long value.
        Assert.assertEquals(new Rational(-1, 2), r1.subtract(1));
        Assert.assertEquals(new Rational(3, 2), r1.subtract(-1));
        Assert.assertEquals(r1, r1.subtract(0));
        Assert.assertEquals(new Rational(-3, 2), r1.subtract(2));
        Assert.assertEquals(new Rational(5, 2), r1.subtract(-2));

        // Try many different whole numbers.
        for (int i = 0; i < 30; ++i) {
            Rational n = new Rational(i);
            Assert.assertEquals(r1.subtract(n), r1.subtract(i));
            Assert.assertEquals(r1.subtract(n), r1.subtract(new BigDecimal(i)));
            Assert.assertEquals(r1.subtract(n),
                                r1.subtract(BigInteger.valueOf(i)));
        }
    }

    /**
     * Test multiplication.
     */
    @Test
    public void multiplication() {
        Rational r1 = new Rational(1, 2);
        Rational r2 = new Rational(1, 2);
        Rational r3 = new Rational(-1, 2);
        Assert.assertEquals(new Rational(1, 4), r1.multiply(r2));
        Assert.assertEquals(new Rational(-1, 4), r1.multiply(r3));
        Assert.assertEquals(new Rational(-1, 4), r3.multiply(r1));

        Rational r4 = new Rational(2, 3);
        Assert.assertEquals(new Rational(1, 3), r1.multiply(r4));
        Assert.assertEquals(new Rational(1, 3), r4.multiply(r1));

        Rational r5 = new Rational(3, 13);
        Rational r6 = new Rational(5, 7);
        Assert.assertEquals(new Rational(15, 91), r5.multiply(r6));
        Assert.assertEquals(new Rational(15, 91), r6.multiply(r5));

        // Multiply a big integer value.
        Assert.assertEquals(r1, r1.multiply(BigInteger.ONE));
        Assert.assertEquals(r1.negate(), r1.multiply(BigInteger.ONE.negate()));
        Assert.assertEquals(Rational.ZERO, r1.multiply(BigInteger.ZERO));
        Assert.assertEquals(Rational.ONE, r1.multiply(new BigInteger("2")));
        Assert.assertEquals(Rational.ONE.negate(),
                            r1.multiply(new BigInteger("-2")));

        // Multiply a big decimal value.
        Assert.assertEquals(r1, r1.multiply(BigDecimal.ONE));
        Assert.assertEquals(r1.negate(), r1.multiply(BigDecimal.ONE.negate()));
        Assert.assertEquals(Rational.ZERO, r1.multiply(BigDecimal.ZERO));
        Assert.assertEquals(Rational.ONE, r1.multiply(new BigDecimal("2")));
        Assert.assertEquals(Rational.ONE.negate(),
                            r1.multiply(new BigDecimal("-2")));
        Assert.assertEquals(new Rational(-5, 4),
                            r1.multiply(new BigDecimal("-2.5")));

        // Multiply a long value.
        Assert.assertEquals(r1, r1.multiply(1));
        Assert.assertEquals(r1.negate(), r1.multiply(-1));
        Assert.assertEquals(Rational.ZERO, r1.multiply(0));
        Assert.assertEquals(Rational.ONE, r1.multiply(2));
        Assert.assertEquals(Rational.ONE.negate(), r1.multiply(-2));

        // Try many different whole numbers.
        for (int i = 0; i < 30; ++i) {
            Rational n = new Rational(i);
            Assert.assertEquals(r1.multiply(n), r1.multiply(i));
            Assert.assertEquals(r1.multiply(n), r1.multiply(new BigDecimal(i)));
            Assert.assertEquals(r1.multiply(n),
                                r1.multiply(BigInteger.valueOf(i)));
        }
    }

    /**
     * Test division.
     */
    @Test
    public void division() {
        Rational r1 = new Rational(1, 2);
        Rational r2 = new Rational(1, 2);
        Rational r3 = new Rational(-1, 2);
        Assert.assertEquals(Rational.ONE, r1.divide(r2));
        Assert.assertEquals(new Rational(-1, 1), r1.divide(r3));
        Assert.assertEquals(new Rational(-1, 1), r3.divide(r1));

        Rational r4 = new Rational(2, 3);
        Assert.assertEquals(new Rational(3, 4), r1.divide(r4));
        Assert.assertEquals(new Rational(4, 3), r4.divide(r1));

        Rational r5 = new Rational(3, 13);
        Rational r6 = new Rational(5, 7);
        Assert.assertEquals(new Rational(21, 65), r5.divide(r6));
        Assert.assertEquals(new Rational(65, 21), r6.divide(r5));

        // Divide a big integer value.
        Assert.assertEquals(r1, r1.divide(BigInteger.ONE));
        Assert.assertEquals(r1.negate(), r1.divide(BigInteger.ONE.negate()));
        Assert.assertEquals(new Rational(1, 4), r1.divide(new BigInteger("2")));
        Assert.assertEquals(new Rational(-1, 4),
                            r1.divide(new BigInteger("-2")));

        // Divide a big decimal value.
        Assert.assertEquals(r1, r1.divide(BigDecimal.ONE));
        Assert.assertEquals(r1.negate(), r1.divide(BigDecimal.ONE.negate()));
        Assert.assertEquals(new Rational(1, 4), r1.divide(new BigDecimal("2")));
        Assert.assertEquals(new Rational(-1, 4),
                            r1.divide(new BigDecimal("-2")));
        Assert.assertEquals(new Rational(-1, 5),
                            r1.divide(new BigDecimal("-2.5")));

        // Divide a long value.
        Assert.assertEquals(r1, r1.divide(1));
        Assert.assertEquals(r1.negate(), r1.divide(-1));
        Assert.assertEquals(new Rational(1, 4), r1.divide(2));
        Assert.assertEquals(new Rational(-1, 4), r1.divide(-2));

        // Try many different whole numbers.
        for (int i = 1; i < 30; ++i) {
            Rational n = new Rational(i);
            Assert.assertEquals(r1.divide(n), r1.divide(i));
            Assert.assertEquals(r1.divide(n), r1.divide(new BigDecimal(i)));
            Assert.assertEquals(r1.divide(n),
                                r1.divide(BigInteger.valueOf(i)));
        }
    }

    /**
     * Division by zero is an error.
     */
    @Test(expected=ArithmeticException.class)
    public void divideByZeroRational() {
        Rational.ONE.divide(Rational.ZERO);
    }

    @Test(expected=ArithmeticException.class)
    public void divideByZeroBigInteger() {
        Rational.ONE.divide(BigInteger.ZERO);
    }

    @Test(expected=ArithmeticException.class)
    public void divideByZeroBigDecimal() {
        Rational.ONE.divide(BigDecimal.ZERO);
    }

    @Test(expected=ArithmeticException.class)
    public void divideByZeroLong() {
        Rational.ONE.divide(0);
    }

    /**
     * Verify the constants associated with the rational class.
     */
    @Test
    public void rationalZero() {
        Assert.assertEquals(BigInteger.ZERO, Rational.ZERO.numerator());
        Assert.assertEquals(BigInteger.ONE, Rational.ZERO.denominator());
    }

    @Test
    public void rationalOne() {
        Assert.assertEquals(BigInteger.ONE, Rational.ONE.numerator());
        Assert.assertEquals(BigInteger.ONE, Rational.ONE.denominator());
    }

    @Test
    public void rationalOneHalf() {
        Assert.assertEquals(BigInteger.ONE, Rational.ONE_HALF.numerator());
        Assert.assertEquals(new BigInteger("2"),
                            Rational.ONE_HALF.denominator());
    }

    /**
     * Any rational times zero should be zero.
     */
    @Test
    public void zeroTimes() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(Rational.ZERO, r.multiply(Rational.ZERO));
            Assert.assertEquals(Rational.ZERO, Rational.ZERO.multiply(r));
        }
    }

    /**
     * Any rational added to zero should be the original rational.
     */
    @Test
    public void zeroAdd() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r, r.add(Rational.ZERO));
            Assert.assertEquals(r, Rational.ZERO.add(r));
            Assert.assertEquals(r, r.add(0));
            Assert.assertEquals(r, r.add(BigInteger.ZERO));
            Assert.assertEquals(r, r.add(BigDecimal.ZERO));
        }
    }

    /**
     * Any rational minus zero should be the original rational.
     */
    @Test
    public void minusZero() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r, r.subtract(Rational.ZERO));
            Assert.assertEquals(r, r.subtract(0));
            Assert.assertEquals(r, r.subtract(BigInteger.ZERO));
            Assert.assertEquals(r, r.subtract(BigDecimal.ZERO));
        }
    }

    /**
     * Zero minus any rational should be the same as the rational times -1.
     */
    @Test
    public void zeroMinus() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r.negate(), Rational.ZERO.subtract(r));
        }
    }

    /**
     * Any rational times one should be the original rational.
     */
    @Test
    public void oneTimes() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r, r.multiply(Rational.ONE));
            Assert.assertEquals(r, Rational.ONE.multiply(r));
            Assert.assertEquals(r, r.multiply(1));
            Assert.assertEquals(r, r.multiply(BigInteger.ONE));
            Assert.assertEquals(r, r.multiply(BigDecimal.ONE));
        }
    }

    /**
     * Any rational divided by one should be the original rational.
     */
    @Test
    public void divideOne() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r, r.divide(Rational.ONE));
            Assert.assertEquals(r, r.divide(1));
            Assert.assertEquals(r, r.divide(BigInteger.ONE));
            Assert.assertEquals(r, r.divide(BigDecimal.ONE));
        }
    }

    /**
     * For non-zero rationals, 1 divided by it will flip the numerator and
     * denominator.
     */
    @Test
    public void oneDivide() {
        for (Rational r: TEST_RATIONALS) {
            if (r.equals(Rational.ZERO)) {
                continue;
            }
            Assert.assertEquals(r.invert(), Rational.ONE.divide(r));
        }
    }

    /**
     * Verify that taking the absolute value works as intended.
     */
    @Test
    public void abs() {
        Assert.assertEquals(Rational.ZERO, Rational.ZERO.abs());
        Assert.assertEquals(Rational.ONE, Rational.ONE.abs());
        Assert.assertEquals(Rational.ONE, new Rational(-1, 1).abs());
        for (int i = -255; i < 255; ++i) {
            for (int j = 1; j < 255; ++j) {
                if (i < 0) {
                    Assert.assertEquals(
                        new Rational(-1 * i, j),
                        new Rational(i, j).abs());
                } else {
                    Assert.assertEquals(
                        new Rational(i, j),
                        new Rational(i, j).abs());
                }
            }
        }
    }

    /**
     * Verify that negation works as intended.
     */
    @Test
    public void negate() {
        Assert.assertEquals(Rational.ZERO, Rational.ZERO.negate());
        Assert.assertEquals(new Rational(-1, 1), Rational.ONE.negate());
        Assert.assertEquals(Rational.ONE, new Rational(-1, 1).negate());
        for (int i = -255; i < 255; ++i) {
            for (int j = 1; j < 255; ++j) {
                Assert.assertEquals(new Rational(-1 * i, j),
                                    new Rational(i, j).negate());
            }
        }
    }

    /**
     * Verify inversion works as intended.
     */
    @Test
    public void invert() {
        Assert.assertEquals(new Rational(1), Rational.ONE.invert());
        Assert.assertEquals(new Rational(2), Rational.ONE_HALF.invert());
        for (Rational r: TEST_RATIONALS) {
            if (r.equals(Rational.ZERO)) {
                continue;
            }
            Rational flipped = new Rational(r.denominator(), r.numerator());
            Assert.assertEquals(flipped, r.invert());
        }
    }

    /**
     * Inversion on zero is an arithmetic exception.
     */
    @Test(expected=ArithmeticException.class)
    public void invertZero() {
        Rational.ZERO.invert();
    }

    /**
     * Verify the floor function works as intended.
     */
    @Test
    public void floor() {
        Assert.assertEquals(BigInteger.ZERO, Rational.ZERO.floor());
        Assert.assertEquals(BigInteger.ZERO, Rational.ONE_HALF.floor());
        Assert.assertEquals(BigInteger.ONE.negate(),
                            Rational.ONE_HALF.negate().floor());
        Assert.assertEquals(BigInteger.ONE, Rational.ONE.floor());
        Assert.assertEquals(BigInteger.ONE.negate(),
                            Rational.ONE.negate().floor());
        Assert.assertEquals(BigInteger.ONE, new Rational("1.1").floor());
        Assert.assertEquals(BigInteger.ONE, new Rational("1.9").floor());
        Assert.assertEquals(new BigInteger("2"), new Rational("2").floor());
        Assert.assertEquals(new BigInteger("2"), new Rational("2.1").floor());
        Assert.assertEquals(new BigInteger("-2"), new Rational("-1.1").floor());
        Assert.assertEquals(new BigInteger("-2"), new Rational("-1.9").floor());
        Assert.assertEquals(new BigInteger("-2"), new Rational("-2.0").floor());
        Assert.assertEquals(new BigInteger("-3"), new Rational("-2.1").floor());
        Assert.assertEquals(BigInteger.ONE.negate(),
                            new Rational(-18, 19).floor());
    }

    /**
     * Verify the ceiling function works as intended.
     */
    @Test
    public void ceiling() {
        Assert.assertEquals(BigInteger.ZERO, Rational.ZERO.ceiling());
        Assert.assertEquals(BigInteger.ONE, Rational.ONE_HALF.ceiling());
        Assert.assertEquals(BigInteger.ZERO,
                            Rational.ONE_HALF.negate().ceiling());
        Assert.assertEquals(BigInteger.ONE, Rational.ONE.ceiling());
        Assert.assertEquals(BigInteger.ONE.negate(),
                            Rational.ONE.negate().ceiling());
        Assert.assertEquals(new BigInteger("2"), new Rational("1.1").ceiling());
        Assert.assertEquals(new BigInteger("2"), new Rational("1.9").ceiling());
        Assert.assertEquals(new BigInteger("2"), new Rational("2").ceiling());
        Assert.assertEquals(new BigInteger("3"),
                            new Rational("2.1").ceiling());
        Assert.assertEquals(new BigInteger("-1"),
                            new Rational("-1.1").ceiling());
        Assert.assertEquals(new BigInteger("-1"),
                            new Rational("-1.9").ceiling());
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-2.0").ceiling());
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-2.1").ceiling());
        Assert.assertEquals(BigInteger.ZERO, new Rational(-18, 19).ceiling());
    }

    /**
     * Verify the round function works as intended.
     */
    @Test
    public void round() {
        Assert.assertEquals(BigInteger.ZERO, Rational.ZERO.round());
        Assert.assertEquals(BigInteger.ZERO, Rational.ONE_HALF.round());
        Assert.assertEquals(BigInteger.ZERO,
                            Rational.ONE_HALF.negate().round());
        Assert.assertEquals(BigInteger.ONE, Rational.ONE.round());
        Assert.assertEquals(BigInteger.ONE.negate(),
                            Rational.ONE.negate().round());
        Assert.assertEquals(new BigInteger("1"), new Rational("1.1").round());
        Assert.assertEquals(new BigInteger("2"), new Rational("1.5").round());
        Assert.assertEquals(new BigInteger("2"), new Rational("1.9").round());
        Assert.assertEquals(new BigInteger("2"), new Rational("2").round());
        Assert.assertEquals(new BigInteger("2"), new Rational("2.1").round());
        Assert.assertEquals(new BigInteger("2"), new Rational("2.5").round());
        Assert.assertEquals(new BigInteger("3"), new Rational("2.6").round());
        Assert.assertEquals(new BigInteger("3"), new Rational("3.4").round());
        Assert.assertEquals(new BigInteger("4"), new Rational("3.5").round());
        Assert.assertEquals(new BigInteger("4"), new Rational("3.9").round());

        Assert.assertEquals(new BigInteger("-1"), new Rational("-1.1").round());
        Assert.assertEquals(new BigInteger("-2"), new Rational("-1.5").round());
        Assert.assertEquals(new BigInteger("-2"), new Rational("-1.9").round());
        Assert.assertEquals(new BigInteger("-2"), new Rational("-2").round());
        Assert.assertEquals(new BigInteger("-2"), new Rational("-2.1").round());
        Assert.assertEquals(new BigInteger("-2"), new Rational("-2.5").round());
        Assert.assertEquals(new BigInteger("-3"), new Rational("-2.6").round());
        Assert.assertEquals(new BigInteger("-3"), new Rational("-3.4").round());
        Assert.assertEquals(new BigInteger("-4"), new Rational("-3.5").round());
        Assert.assertEquals(new BigInteger("-4"), new Rational("-3.9").round());
        Assert.assertEquals(BigInteger.ONE.negate(),
                            new Rational(-18, 19).round());
    }

    /**
     * Test UNNECESSARY rounding mode only works when the number is whole.
     */
    @Test
    public void roundUnnecessaryWhole() {
        Assert.assertEquals(BigInteger.ZERO,
                            Rational.ZERO.round(RoundingMode.UNNECESSARY));
        Assert.assertEquals(BigInteger.ZERO,
                            Rational.ZERO.round(RoundingMode.UNNECESSARY));
        Assert.assertEquals(BigInteger.ONE,
                            Rational.ONE.round(RoundingMode.UNNECESSARY));
        Assert.assertEquals(BigInteger.ONE.negate(),
                            Rational.ONE.negate()
                            .round(RoundingMode.UNNECESSARY));
    }

    @Test(expected=ArithmeticException.class)
    public void roundUnnecessaryPartial1() {
        Rational.ONE_HALF.round(RoundingMode.UNNECESSARY);
    }

    @Test(expected=ArithmeticException.class)
    public void roundUnnecessaryPartial2() {
        new Rational(100, 121).round(RoundingMode.UNNECESSARY);
    }

    @Test(expected=ArithmeticException.class)
    public void roundUnnecessaryPartial3() {
        new Rational(-100, 121).round(RoundingMode.UNNECESSARY);
    }

    /**
     * Test DOWN rounding mode.
     */
    @Test
    public void roundDown() {
        Assert.assertEquals(BigInteger.ZERO,
                            Rational.ZERO.round(RoundingMode.DOWN));
        Assert.assertEquals(new BigInteger("1"),
                            new Rational("1.1").round(RoundingMode.DOWN));
        Assert.assertEquals(new BigInteger("1"),
                            new Rational("1.5").round(RoundingMode.DOWN));
        Assert.assertEquals(new BigInteger("1"),
                            new Rational("1.9").round(RoundingMode.DOWN));
        Assert.assertEquals(new BigInteger("2"),
                            new Rational("2").round(RoundingMode.DOWN));
        Assert.assertEquals(new BigInteger("-1"),
                            new Rational("-1.1").round(RoundingMode.DOWN));
        Assert.assertEquals(new BigInteger("-1"),
                            new Rational("-1.5").round(RoundingMode.DOWN));
        Assert.assertEquals(new BigInteger("-1"),
                            new Rational("-1.9").round(RoundingMode.DOWN));
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-2").round(RoundingMode.DOWN));
        Assert.assertEquals(BigInteger.ZERO,
                            new Rational(-18, 19).round(RoundingMode.DOWN));
    }

    /**
     * Test UP rounding mode.
     */
    @Test
    public void roundUp() {
        Assert.assertEquals(BigInteger.ZERO,
                            Rational.ZERO.round(RoundingMode.UP));
        Assert.assertEquals(new BigInteger("2"),
                            new Rational("1.1").round(RoundingMode.UP));
        Assert.assertEquals(new BigInteger("2"),
                            new Rational("1.5").round(RoundingMode.UP));
        Assert.assertEquals(new BigInteger("2"),
                            new Rational("1.9").round(RoundingMode.UP));
        Assert.assertEquals(new BigInteger("2"),
                            new Rational("2").round(RoundingMode.UP));
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-1.1").round(RoundingMode.UP));
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-1.5").round(RoundingMode.UP));
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-1.9").round(RoundingMode.UP));
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-2").round(RoundingMode.UP));
        Assert.assertEquals(new BigInteger("-1"),
                            new Rational(-18, 19).round(RoundingMode.UP));
    }

    /**
     * Test CEILING rounding mode by comparing it to the ceiling call.
     */
    @Test
    public void roundCeiling() {
        for (int i = -30; i < 30; ++i) {
            Rational r = new Rational(i, 10);
            Assert.assertEquals(r.ceiling(), r.round(RoundingMode.CEILING));
        }
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r.ceiling(), r.round(RoundingMode.CEILING));
        }
    }

    /**
     * Test FLOOR rounding mode by comparing it to the floor call.
     */
    @Test
    public void roundFloor() {
        for (int i = -30; i < 30; ++i) {
            Rational r = new Rational(i, 10);
            Assert.assertEquals(r.floor(), r.round(RoundingMode.FLOOR));
        }
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r.floor(), r.round(RoundingMode.FLOOR));
        }
    }

    /**
     * Test HALF_EVEN rounding mode by comparing it to the round call.
     */
    @Test
    public void roundHalfEven() {
        for (int i = -30; i < 30; ++i) {
            Rational r = new Rational(i, 10);
            Assert.assertEquals(r.round(), r.round(RoundingMode.HALF_EVEN));
        }
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r.round(), r.round(RoundingMode.HALF_EVEN));
        }
    }

    /**
     * Test HALF_DOWN rounding mode.
     */
    @Test
    public void roundHalfDown() {
        Assert.assertEquals(BigInteger.ZERO,
                            Rational.ZERO.round(RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigInteger("1"),
                            new Rational("1.1").round(RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigInteger("1"),
                            new Rational("1.5").round(RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigInteger("2"),
                            new Rational("1.9").round(RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigInteger("2"),
                            new Rational("2").round(RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigInteger("-1"),
                            new Rational("-1.1").round(RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigInteger("-1"),
                            new Rational("-1.5").round(RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-1.9").round(RoundingMode.HALF_DOWN));
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-2").round(RoundingMode.HALF_DOWN));
        Assert.assertEquals(
            BigInteger.ONE.negate(),
            new Rational(-18, 19).round(RoundingMode.HALF_DOWN));
    }

    /**
     * Test HALF_UP rounding mode.
     */
    @Test
    public void roundHalfUp() {
        Assert.assertEquals(BigInteger.ZERO,
                            Rational.ZERO.round(RoundingMode.HALF_UP));
        Assert.assertEquals(new BigInteger("1"),
                            new Rational("1.1").round(RoundingMode.HALF_UP));
        Assert.assertEquals(new BigInteger("2"),
                            new Rational("1.5").round(RoundingMode.HALF_UP));
        Assert.assertEquals(new BigInteger("2"),
                            new Rational("1.9").round(RoundingMode.HALF_UP));
        Assert.assertEquals(new BigInteger("2"),
                            new Rational("2").round(RoundingMode.HALF_UP));
        Assert.assertEquals(new BigInteger("-1"),
                            new Rational("-1.1").round(RoundingMode.HALF_UP));
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-1.5").round(RoundingMode.HALF_UP));
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-1.9").round(RoundingMode.HALF_UP));
        Assert.assertEquals(new BigInteger("-2"),
                            new Rational("-2").round(RoundingMode.HALF_UP));
        Assert.assertEquals(
            BigInteger.ONE.negate(),
            new Rational(-18, 19).round(RoundingMode.HALF_UP));
    }

    /**
     * Verify the mod and floorMod function works as expected.
     */
    @Test
    public void modFloorMod() {
        // Compare against whole numbers.
        for (int i = -30; i <= 30; ++i) {
            for (int j = -30; j <= 30; ++j) {
                if (j == 0) {
                    continue;
                }
                Rational r1 = new Rational(i);
                Rational expected = new Rational(i % j);
                Assert.assertEquals(expected, r1.mod(new Rational(j)));
                Assert.assertEquals(expected, r1.mod(BigInteger.valueOf(j)));
                Assert.assertEquals(expected, r1.mod(new BigDecimal(j)));
                Assert.assertEquals(expected, r1.mod(j));

                if (j < 0 && r1.floorMod(j).longValue() != 0) {
                    Assert.assertEquals(
                        -1, r1.floorMod(new Rational(j)).sign());
                    Assert.assertEquals(
                        -1, r1.floorMod(BigInteger.valueOf(j)).sign());
                    Assert.assertEquals(
                        -1, r1.floorMod(new BigDecimal(j)).sign());
                    Assert.assertEquals(
                        -1, r1.floorMod(j).sign());
                }
                if ((j > 0 && i >= 0) || (j < 0 && i <= 0)) {
                    Assert.assertEquals(expected, r1.floorMod(new Rational(j)));
                    Assert.assertEquals(
                        expected, r1.floorMod(BigInteger.valueOf(j)));
                    Assert.assertEquals(
                        expected, r1.floorMod(new BigDecimal(j)));
                    Assert.assertEquals(expected, r1.floorMod(j));
                }
            }
        }
        // If the mod value and the rational share the same sign, and the mod
        // value is larger, then mod is a no-op.
        for (Rational r: TEST_RATIONALS) {
            long mod = r.round(RoundingMode.UP).longValue();
            if (mod < 0) {
                mod--;
            } else {
                mod++;
            }
            Assert.assertEquals(r, r.mod(new Rational(mod)));
            Assert.assertEquals(r, r.mod(BigInteger.valueOf(mod)));
            Assert.assertEquals(r, r.mod(new BigDecimal(mod)));
            Assert.assertEquals(r, r.floorMod(mod));
            Assert.assertEquals(r, r.floorMod(new Rational(mod)));
            Assert.assertEquals(r, r.floorMod(BigInteger.valueOf(mod)));
            Assert.assertEquals(r, r.floorMod(new BigDecimal(mod)));
            Assert.assertEquals(r, r.floorMod(mod));

            Rational rmod = r.add(1);
            if (mod < 0) {
                rmod = r.subtract(1);
            }
            Assert.assertEquals(r, r.mod(rmod));
            Assert.assertEquals(r, r.floorMod(rmod));
        }
        Rational r1 = new Rational("1.1");
        Rational r2 = new Rational("0.3");
        Assert.assertEquals(new Rational("0.2"), r1.mod(r2));
        Assert.assertEquals(new Rational("0.2"), r1.mod(new BigDecimal("0.3")));
        Assert.assertEquals(new Rational("0.2"), r1.floorMod(r2));
        Assert.assertEquals(new Rational("0.2"), r1.floorMod(
            new BigDecimal("0.3")));

        r2 = new Rational("-0.3");
        Assert.assertEquals(new Rational("0.2"), r1.mod(r2));
        Assert.assertEquals(new Rational("0.2"),
                            r1.mod(new BigDecimal("-0.3")));
        Assert.assertEquals(new Rational("-0.1"), r1.floorMod(r2));
        Assert.assertEquals(new Rational("-0.1"),
                            r1.floorMod(new BigDecimal("-0.3")));

        r1 = new Rational("-1.1");
        r2 = new Rational("0.3");
        Assert.assertEquals(new Rational("-0.2"), r1.mod(r2));
        Assert.assertEquals(new Rational("-0.2"),
                            r1.mod(new BigDecimal("0.3")));
        Assert.assertEquals(new Rational("0.1"), r1.floorMod(r2));
        Assert.assertEquals(new Rational("0.1"), r1.floorMod(
            new BigDecimal("0.3")));

        r2 = new Rational("-0.3");
        Assert.assertEquals(new Rational("-0.2"), r1.mod(r2));
        Assert.assertEquals(new Rational("-0.2"),
                            r1.mod(new BigDecimal("-0.3")));
        Assert.assertEquals(new Rational("-0.2"), r1.floorMod(r2));
        Assert.assertEquals(new Rational("-0.2"),
                            r1.floorMod(new BigDecimal("-0.3")));
    }

    /**
     * Modding by zero is an arithmetic exception.
     */
    @Test(expected=ArithmeticException.class)
    public void modZeroRational() {
        Rational.ONE.mod(Rational.ZERO);
    }

    @Test(expected=ArithmeticException.class)
    public void modZeroBigInteger() {
        Rational.ONE.mod(BigInteger.ZERO);
    }

    @Test(expected=ArithmeticException.class)
    public void modZeroBigDecimal() {
        Rational.ONE.mod(BigDecimal.ZERO);
    }

    @Test(expected=ArithmeticException.class)
    public void modZeroLong() {
        Rational.ONE.mod(0);
    }

    @Test(expected=ArithmeticException.class)
    public void floorModZeroRational() {
        Rational.ONE.floorMod(Rational.ZERO);
    }

    @Test(expected=ArithmeticException.class)
    public void floorModZeroBigInteger() {
        Rational.ONE.floorMod(BigInteger.ZERO);
    }

    @Test(expected=ArithmeticException.class)
    public void floorModZeroBigDecimal() {
        Rational.ONE.floorMod(BigDecimal.ZERO);
    }

    @Test(expected=ArithmeticException.class)
    public void floorModZeroLong() {
        Rational.ONE.floorMod(0);
    }

    /**
     * Verify that min works as intended.
     */
    @Test
    public void min() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r, Rational.min(r));
            List<Rational> list = new ArrayList<>();
            list.add(r);
            Assert.assertEquals(r, Rational.min(list));
        }
        Assert.assertEquals(Rational.ZERO,
                            Rational.min(Rational.ZERO, Rational.ONE_HALF));
        Assert.assertEquals(Rational.ZERO,
                            Rational.min(Rational.ONE_HALF, Rational.ZERO));
        Assert.assertEquals(
            Rational.ZERO,
            Rational.min(Rational.ONE, Rational.ONE_HALF, Rational.ZERO));
        Assert.assertEquals(
            Rational.ZERO,
            Rational.min(Rational.ONE_HALF, Rational.ONE, Rational.ZERO));
        Assert.assertEquals(
            Rational.ONE.negate(),
            Rational.min(Rational.ONE_HALF, Rational.ONE, Rational.ZERO,
                         Rational.ONE.negate(), Rational.ONE_HALF.negate()));
        Assert.assertEquals(
            Rational.ONE_HALF.negate(),
            Rational.min(Rational.ONE_HALF, Rational.ONE, Rational.ZERO,
                         Rational.ONE_HALF.negate()));
    }

    /**
     * Min on empty input is an error.
     */
    @Test(expected=IllegalArgumentException.class)
    public void minEmptyVarArg() {
        Rational.min();
    }

    @Test(expected=IllegalArgumentException.class)
    public void minEmptyList() {
        Rational.min(new ArrayList<Rational>());
    }

    /**
     * Verify that max works as intended.
     */
    @Test
    public void max() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r, Rational.max(r));
            List<Rational> list = new ArrayList<>();
            list.add(r);
            Assert.assertEquals(r, Rational.max(list));
        }
        Assert.assertEquals(Rational.ONE_HALF,
                            Rational.max(Rational.ZERO, Rational.ONE_HALF));
        Assert.assertEquals(Rational.ONE_HALF,
                            Rational.max(Rational.ONE_HALF, Rational.ZERO));
        Assert.assertEquals(
            Rational.ONE,
            Rational.max(Rational.ONE, Rational.ONE_HALF, Rational.ZERO));
        Assert.assertEquals(
            Rational.ONE,
            Rational.max(Rational.ONE_HALF, Rational.ONE, Rational.ZERO));
        Assert.assertEquals(
            Rational.ONE,
            Rational.max(Rational.ONE_HALF, Rational.ONE, Rational.ZERO,
                         Rational.ONE.negate(), Rational.ONE_HALF.negate()));
        Assert.assertEquals(
            Rational.ONE_HALF,
            Rational.max(Rational.ONE_HALF, Rational.ZERO,
                         Rational.ONE_HALF.negate()));
    }

    /**
     * Max on empty input is an error.
     */
    @Test(expected=IllegalArgumentException.class)
    public void maxEmptyVarArg() {
        Rational.max();
    }

    @Test(expected=IllegalArgumentException.class)
    public void maxEmptyList() {
        Rational.max(new ArrayList<Rational>());
    }

    /**
     * Verify the sum function works as expected.
     */
    @Test
    public void sum() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r, Rational.sum(r));
            List<Rational> list = new ArrayList<>();
            list.add(r);
            Assert.assertEquals(r, Rational.sum(list));

            for (Rational other: TEST_RATIONALS) {
                Assert.assertEquals(r.add(other), Rational.sum(r, other));
                List<Rational> l = new ArrayList<>();
                l.add(r);
                l.add(other);
                Assert.assertEquals(r.add(other), Rational.sum(l));
            }
        }
        Assert.assertEquals(Rational.ONE_HALF,
                            Rational.sum(Rational.ZERO, Rational.ONE_HALF));
        Assert.assertEquals(Rational.ONE_HALF,
                            Rational.sum(Rational.ONE_HALF, Rational.ZERO));
        Assert.assertEquals(
            new Rational(3, 2),
            Rational.sum(Rational.ONE, Rational.ONE_HALF, Rational.ZERO));
        Assert.assertEquals(
            new Rational(3, 2),
            Rational.sum(Rational.ONE_HALF, Rational.ONE, Rational.ZERO));
        Assert.assertEquals(
            Rational.ZERO,
            Rational.sum(Rational.ONE_HALF, Rational.ONE, Rational.ZERO,
                         Rational.ONE.negate(), Rational.ONE_HALF.negate()));
        Assert.assertEquals(
            Rational.ZERO,
            Rational.sum(Rational.ONE_HALF, Rational.ZERO,
                         Rational.ONE_HALF.negate()));
    }

    /**
     * Sum on empty input is an error.
     */
    @Test(expected=IllegalArgumentException.class)
    public void sumEmptyVarArg() {
        Rational.sum();
    }

    @Test(expected=IllegalArgumentException.class)
    public void sumEmptyList() {
        Rational.sum(new ArrayList<Rational>());
    }

    /**
     * Verify the product function works as expected.
     */
    @Test
    public void product() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(r, Rational.product(r));
            List<Rational> list = new ArrayList<>();
            list.add(r);
            Assert.assertEquals(r, Rational.product(list));

            for (Rational other: TEST_RATIONALS) {
                Assert.assertEquals(r.multiply(other),
                                    Rational.product(r, other));
                List<Rational> l = new ArrayList<>();
                l.add(r);
                l.add(other);
                Assert.assertEquals(r.multiply(other), Rational.product(l));
            }
        }
        Assert.assertEquals(Rational.ZERO,
                            Rational.product(Rational.ZERO, Rational.ONE_HALF));
        Assert.assertEquals(Rational.ONE_HALF,
                            Rational.product(Rational.ONE_HALF, Rational.ONE));
        Assert.assertEquals(
            new Rational(1, 8),
            Rational.product(Rational.ONE_HALF, Rational.ONE_HALF,
                             Rational.ONE_HALF));
        Assert.assertEquals(
            new Rational(-1, 4),
            Rational.product(Rational.ONE_HALF, Rational.ONE_HALF.negate()));
    }

    /**
     * Product on empty input is an error.
     */
    @Test(expected=IllegalArgumentException.class)
    public void productEmptyVarArg() {
        Rational.product();
    }

    @Test(expected=IllegalArgumentException.class)
    public void productEmptyList() {
        Rational.product(new ArrayList<Rational>());
    }

    /**
     * Verify the pow function works as expected.
     */
    @Test
    public void pow() {
        for (Rational r: TEST_RATIONALS) {
            if (!r.equals(Rational.ZERO)) {
                Assert.assertEquals(r.invert(), r.pow(-1));
                Assert.assertEquals(r.multiply(r).invert(), r.pow(-2));
                Assert.assertEquals(r.multiply(r).multiply(r).invert(),
                                    r.pow(-3));
                Assert.assertEquals(
                    r.multiply(r).multiply(r).multiply(r).invert(),
                    r.pow(-4));
            }
            Assert.assertEquals(Rational.ONE, r.pow(0));
            Assert.assertEquals(r, r.pow(1));
            Assert.assertEquals(r.multiply(r), r.pow(2));
            Assert.assertEquals(r.multiply(r).multiply(r), r.pow(3));
            Assert.assertEquals(r.multiply(r).multiply(r).multiply(r),
                                r.pow(4));
        }
        Assert.assertEquals(Rational.ONE, Rational.ONE.pow(
            Integer.MIN_VALUE));
        Assert.assertEquals(Rational.ONE, Rational.ONE.pow(
            Integer.MAX_VALUE));
        Assert.assertEquals(Rational.ZERO, Rational.ZERO.pow(
            Integer.MAX_VALUE));
    }

    /**
     * Verify the sign function works as expected.
     */
    @Test
    public void sign() {
        Assert.assertEquals(0, Rational.ZERO.sign());
        Assert.assertEquals(1, Rational.ONE_HALF.sign());
        Assert.assertEquals(1, Rational.ONE.sign());
        Assert.assertEquals(-1, Rational.ONE_HALF.negate().sign());
        Assert.assertEquals(-1, Rational.ONE.negate().sign());
        for (Rational r: TEST_RATIONALS) {
            if (r.compareTo(Rational.ZERO) == 0) {
                Assert.assertEquals(0, r.sign());
            } else if (r.compareTo(Rational.ZERO) > 0) {
                Assert.assertEquals(1, r.sign());
            } else {
                Assert.assertEquals(-1, r.sign());
            }
        }
    }

    /**
     * Create identical rationals and verify they hash the same and are equal.
     */
    @Test
    public void hashesEqualsProperly() {
        List<Rational> rationals = new ArrayList<>();
        rationals.add(new Rational(new BigInteger("2000"),
                                   new BigInteger("3000")));
        rationals.add(new Rational(new BigInteger("4"),
                                   new BigInteger("6")));
        rationals.add(new Rational("2000/3000"));
        rationals.add(new Rational("4/6"));
        rationals.add(new Rational(4, 6));
        rationals.add(new Rational(2, 3));
        for (int i = 0; i < rationals.size(); ++i) {
            for (int j = 0; j < rationals.size(); ++j) {
                Rational r1 = rationals.get(i);
                Rational r2 = rationals.get(j);
                Assert.assertEquals(r1, r2);
                Assert.assertEquals(r2, r1);
                Assert.assertEquals(r1.hashCode(), r2.hashCode());
            }
        }

        // Also do a run where we can test the BigDecimal constructor.
        rationals.clear();
        rationals.add(new Rational(new BigDecimal("0.000641")));
        rationals.add(new Rational(new BigInteger("641"),
                                   new BigInteger("1000000")));
        rationals.add(new Rational("641/1000000"));
        rationals.add(new Rational("0.000641"));
        rationals.add(new Rational(641, 1000000));
        for (int i = 0; i < rationals.size(); ++i) {
            for (int j = 0; j < rationals.size(); ++j) {
                Rational r1 = rationals.get(i);
                Rational r2 = rationals.get(j);
                Assert.assertEquals(r1, r2);
                Assert.assertEquals(r2, r1);
                Assert.assertEquals(r1.hashCode(), r2.hashCode());
            }
        }
    }

    /**
     * Slightly different rationals are non-equal.
     */
    @Test
    public void notEqual() {
        Rational r1 = new Rational("10000000000000000000000");
        Rational r2 = new Rational("10000000000000000000001");
        Assert.assertTrue(!r1.equals(r2));
        Assert.assertTrue(!r2.equals(r1));

        r1 = new Rational("1/10000000000000000000000");
        r2 = new Rational("1/10000000000000000000001");
        Assert.assertTrue(!r1.equals(r2));
        Assert.assertTrue(!r2.equals(r1));
    }

    /**
     * Verify comparison is the natural ordering.
     */
    @Test
    public void comparison() {
        Rational r1 = new Rational(-1, 2);
        Rational r2 = new Rational(-2, 3);
        Assert.assertTrue(r1.compareTo(r2) > 0);
        Assert.assertTrue(r2.compareTo(r1) < 0);

        r2 = new Rational(-1, 2);
        Assert.assertEquals(0, r1.compareTo(r2));
        Assert.assertEquals(0, r2.compareTo(r1));

        r2 = new Rational(-1, 4);
        Assert.assertTrue(r1.compareTo(r2) < 0);
        Assert.assertTrue(r2.compareTo(r1) > 0);

        r2 = Rational.ZERO;
        Assert.assertTrue(r1.compareTo(r2) < 0);
        Assert.assertTrue(r2.compareTo(r1) > 0);

        r2 = Rational.ONE;
        Assert.assertTrue(r1.compareTo(r2) < 0);
        Assert.assertTrue(r2.compareTo(r1) > 0);

        r1 = Rational.ZERO;
        r2 = Rational.ZERO;
        Assert.assertEquals(0, r1.compareTo(r2));
        Assert.assertEquals(0, r2.compareTo(r1));

        r2 = new Rational(200, 1);
        Assert.assertTrue(r1.compareTo(r2) < 0);
        Assert.assertTrue(r2.compareTo(r1) > 0);

        r1 = new Rational(123, 100);
        r2 = new Rational(2, 3);
        Assert.assertTrue(r1.compareTo(r2) > 0);
        Assert.assertTrue(r2.compareTo(r1) < 0);

        r2 = new Rational(123, 100);
        Assert.assertEquals(0, r1.compareTo(r2));
        Assert.assertEquals(0, r2.compareTo(r1));

        r2 = new Rational(124, 100);
        Assert.assertTrue(r1.compareTo(r2) < 0);
        Assert.assertTrue(r2.compareTo(r1) > 0);
    }

    /**
     * Test that the toString() method succeeds with various values.
     */
    @Test
    public void toStringZero() {
        Assert.assertEquals("0", Rational.ZERO.toString());
    }

    @Test
    public void toStringOne() {
        Assert.assertEquals("1", Rational.ONE.toString());
    }

    @Test
    public void toStringHalf() {
        Assert.assertEquals("1/2", Rational.ONE_HALF.toString());
    }

    @Test
    public void toStringWholeNumber() {
        Assert.assertEquals("100", new Rational("500/5").toString());
    }

    @Test
    public void toBigDecimalExact() {
        Assert.assertEquals(0, new BigDecimal("1.23").compareTo(
            new Rational(123, 100).bigDecimalValue()));
        Assert.assertEquals(0, new BigDecimal("-1.23").compareTo(
            new Rational(-123, 100).bigDecimalValue()));
        Assert.assertEquals(0, BigDecimal.ZERO.compareTo(
            Rational.ZERO.bigDecimalValue()));
        Assert.assertEquals(0, BigDecimal.ONE.compareTo(
            Rational.ONE.bigDecimalValue()));
    }

    @Test(expected=ArithmeticException.class)
    public void nonterminatingDecimal1() {
        new Rational(1, 3).bigDecimalValue();
    }

    @Test(expected=ArithmeticException.class)
    public void nonterminatingDecimal2() {
        new Rational(1, 3).bigDecimalValue(MathContext.UNLIMITED);
    }

    /**
     * Epsilon to use when comparing double values.
     */
    private static double EPSILON = 0.0000000001;

    @Test
    public void bigDecimalValueWithRounding() {
        BigDecimal d = new Rational(1, 3).bigDecimalValue(
            MathContext.DECIMAL64);
        Assert.assertEquals(0, new BigDecimal("0.3333333333333333")
                            .compareTo(d));
    }

    @Test
    public void bigDecimalValueWithRoundingVerySmall() {
        Rational r = new Rational(
            BigInteger.ONE, new BigInteger("10").pow(5000));
        BigDecimal expected = BigDecimal.ONE.divide(
            new BigDecimal("10").pow(5000));
        // Should be exact, since there is only 1 digit of precision.
        Assert.assertEquals(0, expected.compareTo(r.bigDecimalValue(
            MathContext.DECIMAL32)));
    }

    /**
     * Basis test cases for the Number methods that result in a number
     * between -128 and 127.
     */
    private static List<Rational> SMALL_NUMBERS;
    private static List<Byte> SMALL_RESULTS;
    static {
        Assert.assertEquals(127, Byte.MAX_VALUE);
        Assert.assertEquals(-128, Byte.MIN_VALUE);
        List<Rational> cases = new ArrayList<>();
        List<Byte> results = new ArrayList<>();
        cases.add(Rational.ZERO);
        results.add((byte) 0);

        cases.add(Rational.ONE);
        results.add((byte) 1);

        cases.add(new Rational(1, 2));
        results.add((byte) 0);

        cases.add(new Rational(2, 3));
        results.add((byte) 0);

        cases.add(new Rational(3, 2));
        results.add((byte) 1);

        cases.add(new Rational(9, 3));
        results.add((byte) 3);

        cases.add(new Rational(-1, 3));
        results.add((byte) 0);

        cases.add(new Rational(-2, 3));
        results.add((byte) 0);

        cases.add(new Rational(-4, 3));
        results.add((byte) -1);

        cases.add(new Rational(10000, 79));
        results.add((byte) 126);

        cases.add(new Rational(10000, 79));
        results.add((byte) 126);

        cases.add(new Rational(10080, 79));
        results.add((byte) 127);

        cases.add(new Rational(-10080, 79));
        results.add((byte) -127);

        cases.add(new Rational(-10120, 79));
        results.add((byte) -128);

        SMALL_NUMBERS = Collections.unmodifiableList(cases);
        SMALL_RESULTS = Collections.unmodifiableList(results);
    }

    @Test
    public void byteValue() {
        for (int i = 0; i < SMALL_NUMBERS.size(); ++i) {
            Assert.assertEquals(
                (byte) SMALL_RESULTS.get(i), SMALL_NUMBERS.get(i).byteValue());
        }
    }

    @Test
    public void shortValue() {
        for (int i = 0; i < SMALL_NUMBERS.size(); ++i) {
            Assert.assertEquals(
                (short) SMALL_RESULTS.get(i),
                SMALL_NUMBERS.get(i).shortValue());
        }
        Assert.assertEquals(16500, new Rational(33001, 2).shortValue());
        Assert.assertEquals(-16500, new Rational(-33001, 2).shortValue());
        Assert.assertEquals(Short.MAX_VALUE, new Rational(
            Short.MAX_VALUE, 1).shortValue());
        Assert.assertEquals(Short.MIN_VALUE, new Rational(
            Short.MIN_VALUE, 1).shortValue());
    }

    @Test
    public void intValue() {
        for (int i = 0; i < SMALL_NUMBERS.size(); ++i) {
            Assert.assertEquals(
                (int) SMALL_RESULTS.get(i), SMALL_NUMBERS.get(i).intValue());
        }
        Assert.assertEquals(1000000, new Rational(2000001, 2).intValue());
        Assert.assertEquals(-1000000, new Rational(2000001, -2)
                            .intValue());
        Assert.assertEquals(Integer.MAX_VALUE, new Rational(
            Integer.MAX_VALUE, 1).intValue());
        Assert.assertEquals(Integer.MIN_VALUE, new Rational(
            Integer.MIN_VALUE, 1).intValue());
    }

    @Test
    public void longValue() {
        for (int i = 0; i < SMALL_NUMBERS.size(); ++i) {
            Assert.assertEquals(
                (long) SMALL_RESULTS.get(i), SMALL_NUMBERS.get(i).longValue());
        }
        Assert.assertEquals(1000000000001L, new Rational(2000000000003L, 2)
                            .longValue());
        Assert.assertEquals(-1000000000001L,
                            new Rational(2000000000003L, -2).longValue());
        Assert.assertEquals(Long.MAX_VALUE, new Rational(
            Long.MAX_VALUE, 1).longValue());
        Assert.assertEquals(Long.MIN_VALUE, new Rational(
            Long.MIN_VALUE, 1).longValue());
    }

    /**
     * Basis test cases for the Number methods that result in a number
     * that can be exactly represented as a float.
     */
    private static List<Rational> FLOAT_NUMBERS;
    private static List<Float> FLOAT_RESULTS;
    static {
        List<Rational> cases = new ArrayList<>();
        List<Float> results = new ArrayList<>();
        cases.add(Rational.ZERO);
        results.add(0.0f);

        cases.add(Rational.ONE);
        results.add(1.0f);

        cases.add(new Rational(1, 2));
        results.add(0.5f);

        cases.add(new Rational(3, 4));
        results.add(0.75f);

        cases.add(new Rational(3, 2));
        results.add(1.5f);

        cases.add(new Rational(9, 3));
        results.add(3.0f);

        cases.add(new Rational(-1, 2));
        results.add(-0.5f);

        cases.add(new Rational(-3, 4));
        results.add(-0.75f);

        cases.add(new Rational(-4, 2));
        results.add(-2.0f);

        cases.add(new Rational(78125, 10000000));
        results.add(0.0078125f);

        cases.add(new Rational(-78125, 10000000));
        results.add(-0.0078125f);

        cases.add(new Rational(new BigDecimal(Float.MIN_VALUE)));
        results.add(Float.MIN_VALUE);

        cases.add(new Rational(new BigDecimal(Float.MIN_NORMAL)));
        results.add(Float.MIN_NORMAL);

        cases.add(new Rational(new BigDecimal(Float.MIN_VALUE)
                               .multiply(new BigDecimal("-1"))));
        results.add(-1.0f * Float.MIN_VALUE);

        cases.add(new Rational(new BigDecimal(Float.MIN_NORMAL)
                               .multiply(new BigDecimal("-1"))));
        results.add(-1.0f * Float.MIN_NORMAL);

        cases.add(new Rational(new BigDecimal(Float.MAX_VALUE)));
        results.add(Float.MAX_VALUE);

        cases.add(new Rational(new BigDecimal(Float.MAX_VALUE)
                               .multiply(new BigDecimal("-1"))));
        results.add(-1.0f * Float.MAX_VALUE);

        FLOAT_NUMBERS = Collections.unmodifiableList(cases);
        FLOAT_RESULTS = Collections.unmodifiableList(results);
    }

    @Test
    public void floatValue() {
        for (int i = 0; i < FLOAT_NUMBERS.size(); ++i) {
            Assert.assertEquals(
                new Float(new BigDecimal(FLOAT_RESULTS.get(i)).floatValue()),
                new Float(FLOAT_NUMBERS.get(i).floatValue()));
        }
        Rational positiveExceeds = new Rational(
            new BigDecimal(Float.MAX_VALUE).multiply(new BigDecimal("2")));
        Assert.assertEquals(new Float(Float.POSITIVE_INFINITY),
                            new Float(positiveExceeds.floatValue()));
        Rational negativeExceeds = positiveExceeds.multiply(
            new Rational(-1, 1));
        Assert.assertEquals(new Float(Float.NEGATIVE_INFINITY),
                            new Float(negativeExceeds.floatValue()));
        // Test with some rationals that have repeating decimal
        // expansions.
        Rational r = new Rational(3227, 555);
        // Use a math context so large that it will definitely hold enough
        // digits for an accurate floatValue() conversion.
        MathContext mc = new MathContext(200, RoundingMode.DOWN);
        BigDecimal c = new BigDecimal(3227).divide(new BigDecimal(555), mc);
        Assert.assertEquals(
            new Float(c.floatValue()),
            new Float(r.floatValue()));
        for (int denom = 1; denom < 10000; ++denom) {
            r = new Rational(1, denom);
            c = BigDecimal.ONE.divide(new BigDecimal(denom), mc);
            Assert.assertEquals(
                new Float(c.floatValue()),
                new Float(r.floatValue()));
            r = new Rational(-1, denom);
            c = c.multiply(new BigDecimal("-1"));
            Assert.assertEquals(
                new Float(c.floatValue()),
                new Float(r.floatValue()));
        }
    }

    /**
     * Verify underflow behavior with floatValue().
     */
    @Test
    public void floatUnderflow() {
        StringBuilder b = new StringBuilder();
        b.append("1");
        for (int i = 0; i < 1000; ++i) {
            b.append("0");
        }
        Rational r = new Rational(BigInteger.ONE, new BigInteger(b.toString()));
        Assert.assertEquals(
            new Float(0.0f),
            new Float(r.floatValue()));
    }

    /**
     * Additional test cases for double conversion.
     */
    private static List<Rational> DOUBLE_NUMBERS;
    private static List<Double> DOUBLE_RESULTS;
    static {
        List<Rational> cases = new ArrayList<>();
        List<Double> results = new ArrayList<>();
        double mid = Float.MAX_VALUE + 100.0;
        cases.add(new Rational(new BigDecimal(mid)));
        results.add(mid);

        cases.add(new Rational(new BigDecimal(-1.0 * mid)));
        results.add(-1.0 * mid);

        cases.add(new Rational(new BigDecimal(Double.MIN_VALUE)));
        results.add(Double.MIN_VALUE);

        cases.add(new Rational(new BigDecimal(Double.MIN_NORMAL)));
        results.add(Double.MIN_NORMAL);

        cases.add(new Rational(new BigDecimal(Double.MIN_VALUE)
                               .multiply(new BigDecimal("-1"))));
        results.add(-1.0 * Double.MIN_VALUE);

        cases.add(new Rational(new BigDecimal(Double.MIN_NORMAL)
                               .multiply(new BigDecimal("-1"))));
        results.add(-1.0 * Double.MIN_NORMAL);

        cases.add(new Rational(new BigDecimal(Double.MAX_VALUE)));
        results.add(Double.MAX_VALUE);

        cases.add(new Rational(new BigDecimal(Double.MAX_VALUE)
                               .multiply(new BigDecimal("-1"))));
        results.add(-1.0 * Double.MAX_VALUE);

        DOUBLE_NUMBERS = Collections.unmodifiableList(cases);
        DOUBLE_RESULTS = Collections.unmodifiableList(results);
    }

    @Test
    public void doubleValue() {
        for (int i = 0; i < FLOAT_NUMBERS.size(); ++i) {
            Assert.assertEquals(
                new Double(new BigDecimal(FLOAT_RESULTS.get(i)).doubleValue()),
                new Double(FLOAT_NUMBERS.get(i).doubleValue()));
        }
        for (int i = 0; i < DOUBLE_NUMBERS.size(); ++i) {
            Assert.assertEquals(
                new Double(new BigDecimal(DOUBLE_RESULTS.get(i)).doubleValue()),
                new Double(DOUBLE_NUMBERS.get(i).doubleValue()));
        }
        Rational positiveExceeds = new Rational(
            new BigDecimal(Double.MAX_VALUE).multiply(new BigDecimal("2")));
        Assert.assertEquals(new Double(Double.POSITIVE_INFINITY),
                            new Double(positiveExceeds.doubleValue()));
        Rational negativeExceeds = positiveExceeds.multiply(
            new Rational(-1, 1));
        Assert.assertEquals(new Double(Double.NEGATIVE_INFINITY),
                            new Double(negativeExceeds.floatValue()));
        // Test with some rationals that have repeating decimal
        // expansions.
        Rational r = new Rational(3227, 555);
        // Use a math context so large that it will definitely hold enough
        // digits for an accurate doubleValue() conversion.
        MathContext mc = new MathContext(400, RoundingMode.DOWN);
        BigDecimal c = new BigDecimal(3227).divide(new BigDecimal(555), mc);
        Assert.assertEquals(
            new Double(c.doubleValue()),
            new Double(r.doubleValue()));
        for (int denom = 1; denom < 10000; ++denom) {
            r = new Rational(1, denom);
            c = BigDecimal.ONE.divide(new BigDecimal(denom), mc);
            Assert.assertEquals(
                new Double(c.doubleValue()),
                new Double(r.doubleValue()));
                c = c.multiply(new BigDecimal("-1"));
                r = new Rational(-1, denom);
            Assert.assertEquals(
                new Double(c.doubleValue()),
                new Double(r.doubleValue()));
        }
    }

    /**
     * Verify underflow behavior with doubleValue().
     */
    @Test
    public void doubleUnderflow() {
        StringBuilder b = new StringBuilder();
        b.append("1");
        for (int i = 0; i < 1000; ++i) {
            b.append("0");
        }
        Rational r = new Rational(BigInteger.ONE, new BigInteger(b.toString()));
        Assert.assertEquals(
            new Double(0.0),
            new Double(r.doubleValue()));
    }
}
