/*
The MIT License (MIT)

Copyright (c) 2015 Elliott Karpilovsky

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package com.ekarp.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

public class RationalStressTest {

    /**
     * Approximately how many random objects to generate for each type of
     * behavior we want to test, e.g., small rationals, large rationals, etc.
     */
    private static final int NUM_ELEMS = 1000;

    /**
     * List of rationals to use whenever a rational needs to be tested. Includes
     * hard coded cases and fuzzed cases. Immutable.
     */
    private static final List<Rational> TEST_RATIONALS;

    /**
     * List of lists of rationals, for use in sum/product tests. All lists
     * are immutable.
     */
    private static final List<List<Rational>> TEST_RATIONAL_LISTS;

    /**
     * List of shorts to use whenever needed for testing. Includes hard coded
     * cases and fuzzed cases. Immutable.
     */
    private static final List<Short> TEST_SHORTS;

    /**
     * List of integers to use whenever needed for testing. Includes hard coded
     * cases and fuzzed cases. Immutable.
     */
    private static final List<Integer> TEST_INTEGERS;

    /**
     * List of longs to use whenever needed for testing. Includes hard coded
     * cases and fuzzed cases. Immutable.
     */
    private static final List<Long> TEST_LONGS;

    /**
     * List of floats to use whenever needed for testing. Includes hard coded
     * cases and fuzzed cases. Immutable.
     */
    private static final List<Float> TEST_FLOATS;

    /**
     * List of doubles to use whenever needed for testing. Includes hard coded
     * cases and fuzzed cases. Immutable.
     */
    private static final List<Double> TEST_DOUBLES;

    /**
     * List of BigIntegers to use whenever needed for testing. Includes hard
     * coded cases and fuzzed cases. Immutable.
     */
    private static final List<BigInteger> TEST_BIG_INTEGERS;

    /**
     * List of BigDecimals to use whenever needed for testing. Includes hard
     * coded cases and fuzzed cases. Immutable.
     */
    private static final List<BigDecimal> TEST_BIG_DECIMALS;

    static {
        // The random seed to use for generating all the fuzzed objects.
        byte[] b = new SecureRandom().generateSeed(8);
        long seed = ByteBuffer.wrap(b).getLong();
        System.out.printf("Seed for run: %d\n", seed);
        Random random = new Random(seed);

        // Randomize the order of each list, so if iterations through
        // pairs of lists are done, the two elements have less of a chance
        // of being drawn from the same distribution. For the same reason, these
        // generators often generate multiple duplicates.
        List<Rational> rationals = genTestRationals(random, NUM_ELEMS);
        Collections.shuffle(rationals, random);
        TEST_RATIONALS = Collections.unmodifiableList(rationals);

        List<List<Rational>> origRationalLists = genTestRationalLists(
            random, NUM_ELEMS);
        List<List<Rational>> rationalLists = new ArrayList<>();
        for (List<Rational> r: origRationalLists) {
            Collections.shuffle(r, random);
            rationalLists.add(Collections.unmodifiableList(new ArrayList<>(r)));
        }
        Collections.shuffle(rationalLists, random);
        TEST_RATIONAL_LISTS = Collections.unmodifiableList(rationalLists);

        List<Short> shorts = genTestShorts(random, NUM_ELEMS);
        Collections.shuffle(shorts);
        TEST_SHORTS = Collections.unmodifiableList(shorts);

        List<Integer> integers = genTestInts(random, NUM_ELEMS);
        Collections.shuffle(integers);
        TEST_INTEGERS = Collections.unmodifiableList(integers);

        List<Long> longs = genTestLongs(random, NUM_ELEMS);
        Collections.shuffle(longs);
        TEST_LONGS = Collections.unmodifiableList(longs);

        List<Float> floats = genTestFloats(random, NUM_ELEMS);
        Collections.shuffle(floats);
        TEST_FLOATS = Collections.unmodifiableList(floats);

        List<Double> doubles = genTestDoubles(random, NUM_ELEMS);
        Collections.shuffle(doubles);
        TEST_DOUBLES = Collections.unmodifiableList(doubles);

        List<BigInteger> bigInts = genTestBigIntegers(random, NUM_ELEMS);
        Collections.shuffle(bigInts);
        TEST_BIG_INTEGERS = Collections.unmodifiableList(bigInts);

        List<BigDecimal> bigDecs = genTestBigDecimals(random, NUM_ELEMS);
        Collections.shuffle(bigDecs);
        TEST_BIG_DECIMALS = Collections.unmodifiableList(bigDecs);
    }

    /**
     * Generates a list of rationals to test.
     *
     * @param random the random number generator
     * @param numElems approximate number of elements to generate per type of
     *        rational (e.g., small, large)
     * @return a list of rational numbers to test, some with hardcoded
     *         values and some fuzzed
     */
    private static List<Rational> genTestRationals(
        Random random, int numElems) {
        List<Rational> result = new ArrayList<>();
        for (int i = 0; i < numElems; ++i) {
            result.add(Rational.ZERO);
            result.add(Rational.ONE);
            result.add(Rational.ONE.negate());
            result.add(Rational.ONE_HALF);
            result.add(Rational.ONE_HALF.negate());
            // Rationals with a numerator of +/- 1.
            result.add(new Rational(1, i + 1));
            result.add(new Rational(-1, i + 1));
            // Rationals with a denominator of 1.
            result.add(new Rational(i, 1));
            result.add(new Rational(-1 * i, 1));
            // Rationals with random values drawn from various sizes.
            for (int j = 8; j < 17; ++j) {
                result.add(randomRational(random, j, j));
            }
            result.add(randomRational(random, 32, 32));
            result.add(randomRational(random, 64, 64));
            result.add(randomRational(random, 128, 128));
            // Rationals with a random size.
            result.add(randomRational(random, random.nextInt(32),
                                      random.nextInt(32) + 1));
            result.add(randomRational(random, random.nextInt(1000),
                                      random.nextInt(1000) + 1));
            // Rationals with numerator/denominator close to Float.MAX_VALUE.
            result.add(randomRational(random,
                                      Float.MAX_EXPONENT,
                                      Float.MAX_EXPONENT));
            // Rationals with numerator/denominator close to Double.MAX_VALUE.
            result.add(randomRational(random,
                                      Double.MAX_EXPONENT,
                                      Double.MAX_EXPONENT));
            // Rationals up to around Float.MAX_VALUE or Double.MAX_VALUE
            // (denominators are very small).
            result.add(randomRational(random, Float.MAX_EXPONENT, 4));
            result.add(randomRational(random, Double.MAX_EXPONENT, 4));
            // Rationals that can exceed Float.MAX_VALUE or Double.MAX_VALUE.
            result.add(
                randomRational(random, 2 * Float.MAX_EXPONENT,
                               Float.MAX_EXPONENT / 4));
            result.add(randomRational(random, 1 + Float.MAX_EXPONENT, 4));
            result.add(
                randomRational(random, 2 * Double.MAX_EXPONENT,
                               Double.MAX_EXPONENT / 4));
            result.add(randomRational(random, 1 + Double.MAX_EXPONENT, 4));
        }
        return result;
    }

    /**
     * Generates a list of lists of rationals to test.
     *
     * @param random the random number generator
     * @param numElems approximate number of elements to generate per type of
     *        rational (e.g., small, large)
     * @return a list of lists of rational numbers to test, some with hardcoded
     *         values and some fuzzed; inner lists are immutable
     */
    private static List<List<Rational>> genTestRationalLists(
        Random random, int numElems) {
        List<List<Rational>> result = new ArrayList<>();
        for (int i = 0; i < numElems; ++i) {
            int size = random.nextInt(20);
            List<Rational> l = new ArrayList<>();
            for (int j = 0; j < size; ++j) {
                l.add(TEST_RATIONALS.get(
                    random.nextInt(TEST_RATIONALS.size())));
            }
            result.add(l);
        }
        return result;
    }

    /**
     * Generate a random rational.
     *
     * @param random the random generator to use
     * @param numNumerBits number of bits in the numerator
     * @param numDenomBits number of bits in the denominator
     * @return a random rational
     */
    private static Rational randomRational(
        Random random, int numNumerBits, int numDenomBits) {
        Assert.assertTrue(numDenomBits > 0);
        BigInteger numer = randomBigInteger(random, numNumerBits);
        BigInteger denom = randomBigInteger(random, numDenomBits);
        while (denom.equals(BigInteger.ZERO)) {
            denom = randomBigInteger(random, numDenomBits);
        }
        return new Rational(numer, denom);
    }


    /**
     * Generate a list of shorts for testing.
     *
     * @param random the random generator
     * @param numElems approximate number of elements to generate per type of
     *        short (e.g., small, large)
     * @return a list of shorts for testing
     */
    private static List<Short> genTestShorts(Random random, int numElems) {
        List<Short> result = new ArrayList<>();
        for (int i = 0; i < numElems; ++i) {
            result.add((short) 0);
            result.add(Short.MIN_VALUE);
            result.add(Short.MAX_VALUE);
            result.add((short) i);
            result.add((short) (-1 * i));
            result.add((short) random.nextInt());
            result.add((short) random.nextInt());
            result.add((short) random.nextInt());
            result.add((short) random.nextInt());
            result.add((short) random.nextInt());
        }
        return result;
    }

    /**
     * Generate a list of integers for testing.
     *
     * @param random the random generator
     * @param numElems approximate number of elements to generate per type of
     *        integer (e.g., small, large)
     * @return a list of integers for testing
     */
    private static List<Integer> genTestInts(Random random, int numElems) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < numElems; ++i) {
            result.add(0);
            result.add(Integer.MAX_VALUE);
            result.add(Integer.MIN_VALUE);
            result.add(i);
            result.add(-1 * i);
            result.add(random.nextInt());
            result.add(random.nextInt());
            result.add(random.nextInt());
            result.add(random.nextInt());
            result.add(random.nextInt());
        }
        return result;
    }

    /**
     * Generate a list of longs for testing.
     *
     * @param random the random generator
     * @param numElems approximate number of elements to generate per type of
     *        integer (e.g., small, large)
     * @return a list of longs for testing
     */
    private static List<Long> genTestLongs(Random random, int numElems) {
        List<Long> result = new ArrayList<>();
        for (long i = 0; i < numElems; ++i) {
            result.add((long) 0);
            result.add((long) i);
            result.add((long) -1 * i);
            result.add(Long.MAX_VALUE);
            result.add(Long.MIN_VALUE);
            result.add(random.nextLong());
            result.add(random.nextLong());
            result.add(random.nextLong());
            result.add(random.nextLong());
            result.add(random.nextLong());
        }
        return result;
    }

    /**
     * Generate a list of floats for testing.
     *
     * @param random the random generator
     * @param numElems approximate number of elements to generate per type of
     *        float (e.g., small, large)
     * @return a list of floats for testing
     */
    private static List<Float> genTestFloats(Random random, int numElems) {
        List<Float> result = new ArrayList<>();
        for (int i = 0; i < numElems; ++i) {
            result.add(Float.NaN);
            result.add(Float.POSITIVE_INFINITY);
            result.add(Float.NEGATIVE_INFINITY);
            result.add(Float.MIN_VALUE);
            result.add(Float.MAX_VALUE);
            result.add(Float.MIN_NORMAL);
            result.add(0.0f);
            result.add(random.nextFloat());
            result.add(random.nextFloat());
            result.add(random.nextFloat());
            result.add(random.nextFloat());
            result.add(random.nextFloat());
            result.add(Float.intBitsToFloat(random.nextInt()));
            result.add(Float.intBitsToFloat(random.nextInt()));
            result.add(Float.intBitsToFloat(random.nextInt()));
            result.add(Float.intBitsToFloat(random.nextInt()));
            result.add(Float.intBitsToFloat(random.nextInt()));
        }
        return result;
    }

    /**
     * Generate a list of doubles for testing.
     *
     * @param random the random generator
     * @param numElems approximate number of elements to generate per type of
     *        double (e.g., small, large)
     * @return a list of doubles for testing
     */
    private static List<Double> genTestDoubles(Random random, int numElems) {
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < numElems; ++i) {
            result.add(0.0);
            result.add(Double.NaN);
            result.add(Double.POSITIVE_INFINITY);
            result.add(Double.NEGATIVE_INFINITY);
            result.add(Double.MIN_VALUE);
            result.add(Double.MAX_VALUE);
            result.add(Double.MIN_NORMAL);
            result.add(random.nextDouble());
            result.add(random.nextDouble());
            result.add(random.nextDouble());
            result.add(random.nextDouble());
            result.add(random.nextDouble());
            result.add(Double.longBitsToDouble(random.nextInt()));
            result.add(Double.longBitsToDouble(random.nextLong()));
            result.add(Double.longBitsToDouble(random.nextLong()));
            result.add(Double.longBitsToDouble(random.nextLong()));
            result.add(Double.longBitsToDouble(random.nextLong()));
        }
        return result;
    }

    /**
     * Generate a list of big integers for testing.
     *
     * @param random the random generator
     * @param numElems approximate number of elements to generate per type of
     *        big integer (e.g., small, large)
     * @return a list of big integers for testing
     */
    private static List<BigInteger> genTestBigIntegers(
        Random random, int numElems) {
        List<BigInteger> result = new ArrayList<>();
        for (int i = 0; i < numElems; ++i) {
            result.add(BigInteger.ZERO);
            result.add(BigInteger.ONE);
            result.add(BigInteger.ONE.negate());
            result.add(BigInteger.valueOf(i));
            result.add(BigInteger.valueOf(-1 * i));
            result.add(randomBigInteger(random, 29));
            result.add(randomBigInteger(random, 30));
            result.add(randomBigInteger(random, 31));
            result.add(randomBigInteger(random, 32));
            result.add(randomBigInteger(random, 33));
            result.add(randomBigInteger(random, 34));
        }
        return result;
    }

    /**
     * Generate a random big integer (possibly negative).
     *
     * @param numBits number of bits in the result
     * @param random the random generator to use
     * @return a random big integer
     */
    private static BigInteger randomBigInteger(Random random, int numBits) {
        BigInteger n = new BigInteger(numBits, random);
        if (random.nextBoolean()) {
            return n.negate();
        }
        return n;
    }

    /**
     * Generate a list of big decimals for testing.
     *
     * @param random the random generator
     * @param numElems approximate number of elements to generate per type of
     *        big decimal (e.g., small, large)
     * @return a list of big decimals for testing
     */
    private static List<BigDecimal> genTestBigDecimals(
        Random random, int numElems) {
        List<BigDecimal> result = new ArrayList<>();
        for (BigInteger bi: genTestBigIntegers(random, numElems)) {
            result.add(new BigDecimal(bi));
        }
        List<BigInteger> l1 = genTestBigIntegers(random, numElems);
        Collections.shuffle(l1, random);
        List<BigInteger> l2 = genTestBigIntegers(random, numElems);
        Collections.shuffle(l2, random);
        for (int i = 0; i < numElems; ++i) {
            result.add(new BigDecimal(l1.get(i).toString() + "." +
                                      l2.get(i).abs().toString()));
        }
        return result;
    }

    /**
     * Test conversion to and from bytes.
     */
    @Test
    public void allBytesConversion() {
        for (byte n = Byte.MIN_VALUE; n < Byte.MAX_VALUE; ++n) {
            for (byte d = Byte.MIN_VALUE; d < Byte.MAX_VALUE; ++d) {
                if (d == 0) {
                    continue;
                }
                Rational r = new Rational(n, d);
                Assert.assertEquals((byte) (n / d), r.byteValue());
            }
        }
    }

    /**
     * Test conversion to and from shorts.
     */
    @Test
    public void shortsConversion() {
        for (int i = 0; i < TEST_SHORTS.size() - 1; ++i) {
            short n = TEST_SHORTS.get(i);
            short d = TEST_SHORTS.get(i + 1);
            if (d == 0) {
                continue;
            }
            Rational r = new Rational(n, d);
            Assert.assertEquals((short) (n / d), r.shortValue());
        }
    }

    /**
     * Test conversion to and from ints.
     */
    @Test
    public void intsConversion() {
        for (int i = 0; i < TEST_INTEGERS.size() - 1; ++i) {
            int n = TEST_INTEGERS.get(i);
            int d = TEST_INTEGERS.get(i + 1);
            if (d == 0) {
                continue;
            }
            Rational r = new Rational(n, d);
            Assert.assertEquals(n / d, r.intValue());
        }
    }

    /**
     * Test conversion to and from longs.
     */
    @Test
    public void longsConversion() {
        for (int i = 0; i < TEST_LONGS.size() - 1; ++i) {
            long n = TEST_LONGS.get(i);
            long d = TEST_LONGS.get(i + 1);
            if (d == 0) {
                continue;
            }
            Rational r = new Rational(n, d);
            Assert.assertEquals(n / d, r.longValue());
            r = new Rational(n);
            Assert.assertEquals(n, r.longValue());
        }
    }

    /**
     * Test conversion to and from floats.
     */
    @Test
    public void floatsConversion() {
        for (Float f: TEST_FLOATS) {
            if (Float.isNaN(f) || Float.isInfinite(f)) {
                continue;
            }
            BigDecimal d = new BigDecimal(f);
            Rational r = new Rational(d);
            Assert.assertEquals(
                new Float(f), new Float(r.floatValue()));
        }
    }

    /**
     * Test conversion to and from doubles.
     */
    @Test
    public void doublesConversion() {
        for (Double f: TEST_DOUBLES) {
            if (Double.isNaN(f) || Double.isInfinite(f)) {
                continue;
            }
            BigDecimal d = new BigDecimal(f);
            Rational r = new Rational(d);
            Assert.assertEquals(
                new Double(f), new Double(r.doubleValue()));
        }
    }

    /**
     * Test conversion to and from strings.
     */
    @Test
    public void toFromString() {
        for (Rational r: TEST_RATIONALS) {
            Assert.assertEquals(new Rational(r.toString()), r);
        }
        for (BigInteger b1: TEST_BIG_INTEGERS) {
            Assert.assertEquals(new Rational(b1), new Rational(b1.toString()));
            Assert.assertEquals(b1.toString(), new Rational(b1).toString());
        }
        for (int i = 0; i < TEST_BIG_INTEGERS.size() - 1; ++i) {
            BigInteger b1 = TEST_BIG_INTEGERS.get(i);
            BigInteger b2 = TEST_BIG_INTEGERS.get(i + 1);
            if (b2.equals(BigInteger.ZERO)) {
                continue;
            }
            if (b2.equals(BigInteger.ONE)) {
                Assert.assertEquals(
                    new Rational(b1, b2), new Rational(b1.toString()));
                Assert.assertEquals(
                    new Rational(b1, b2),
                    new Rational(b1.toString() + "/" + b2.toString()));
                Assert.assertEquals(
                    b1.toString(), new Rational(b1, b2).toString());
            }
            else {
                Assert.assertEquals(
                    new Rational(b1, b2.abs()),
                    new Rational(b1.toString() + "/" + b2.abs().toString()));
            }
        }
        for (BigDecimal b: TEST_BIG_DECIMALS) {
            Assert.assertEquals(new Rational(b), new Rational(b.toString()));
            Assert.assertEquals(new Rational(b),
                                new Rational(b.toPlainString()));
        }
    }

    /**
     * For a rational, the float and double equivalents of them should be very
     * close in value to the floating point arithmetic of the float versions
     * of their numerators and denominators.
     */
    @Test
    public void floatConversionClose() {
        for (Rational r: TEST_RATIONALS) {
            if (Float.isInfinite(r.numerator().floatValue()) ||
                Float.isInfinite(r.denominator().floatValue())) {
                continue;
            }
            assertFloatTolerance(
                r.numerator().floatValue() / r.denominator().floatValue(),
                r.floatValue());
        }
    }

    @Test
    public void doubleConversionClose() {
        for (Rational r: TEST_RATIONALS) {
            if (Double.isInfinite(r.numerator().doubleValue()) ||
                Double.isInfinite(r.denominator().doubleValue())) {
                continue;
            }
            assertDoubleTolerance(
                r.numerator().doubleValue() / r.denominator().doubleValue(),
                r.doubleValue());
        }
    }

    /**
     * For a rational, the float and double equivalents should be exactly the
     * same as doing a BigDecimal division with an even larger precision
     * (and rounding down), followed by a conversion to float and double.
     */
    @Test
    public void floatConversionSameAsBigDecimal() {
        for (Rational r: TEST_RATIONALS) {
            // Create a big decimal representation that will use a very
            // high precision, verify it is equivalent to our implementation
            // that uses less (but sufficient) precision.
            BigDecimal c = new BigDecimal(r.numerator()).divide(
                new BigDecimal(r.denominator()),
                new MathContext(100 + r.numerator().bitLength() +
                                r.denominator().bitLength(),
                                RoundingMode.DOWN));
            Assert.assertEquals(
                new Float(c.floatValue()), new Float(r.floatValue()));
            Assert.assertEquals(
                new Float(c.doubleValue()), new Float(r.doubleValue()));
        }
    }

    /**
     * Verify arithmetic operations are the same as the big decimal versions.
     */
    @Test
    public void arithmeticBigDecimal() {
        for (int i = 0; i < TEST_BIG_DECIMALS.size() - 1; ++i) {
            BigDecimal b1 = TEST_BIG_DECIMALS.get(i);
            BigDecimal b2 = TEST_BIG_DECIMALS.get(i + 1);
            Rational r1 = new Rational(b1);
            Rational r2 = new Rational(b2);
            Assert.assertEquals(new Rational(b1.add(b2)), r1.add(r2));
            Assert.assertEquals(new Rational(b1.add(b2)), r1.add(b2));
            Assert.assertEquals(new Rational(b1.subtract(b2)),
                                r1.subtract(r2));
            Assert.assertEquals(new Rational(b1.subtract(b2)),
                                r1.subtract(b2));
            Assert.assertEquals(new Rational(b1.multiply(b2)),
                                r1.multiply(r2));
            Assert.assertEquals(new Rational(b1.multiply(b2)),
                                r1.multiply(b2));
            if (b2.compareTo(BigDecimal.ZERO) == 0) {
                try {
                    r1.divide(r2);
                    Assert.fail("Should raise exception");
                }
                catch (ArithmeticException e) {}
                try {
                    r1.divide(b2);
                    Assert.fail("Should raise exception");
                }
                catch (ArithmeticException e) {}
                continue;
            }
            try {
                BigDecimal b3 = b1.divide(b2);
                Assert.assertEquals(new Rational(b3), r1.divide(r2));
            } catch (ArithmeticException e) {}
            Assert.assertEquals(r1.divide(b2), r1.divide(r2));
        }
    }

    /**
     * Verify arithmetic operations on integers are the same as rationals.
     */
    @Test
    public void arithmeticInteger() {
        for (int index = 0; index < TEST_INTEGERS.size() - 1; ++index) {
            int i1 = TEST_INTEGERS.get(index);
            int j1 = TEST_INTEGERS.get(index + 1);
            Rational r1 = new Rational(i1);
            Rational r2 = new Rational(j1);
            // Do the math in longs, so there is no overflow.
            long i = i1;
            long j = j1;
            Assert.assertEquals(i + j, r1.add(r2).longValue());
            Assert.assertEquals(i + j, r1.add(j).longValue());

            Assert.assertEquals(i - j, r1.subtract(r2).longValue());
            Assert.assertEquals(i - j, r1.subtract(j).longValue());

            Assert.assertEquals(i * j, r1.multiply(r2).longValue());
            Assert.assertEquals(i * j, r1.multiply(j).longValue());
        }
    }

    /**
     * Verify arithmetic operations on doubles are very close to the same
     * operations as rationals.
     */
    @Test
    public void arithmeticDouble() {
        for (int i = 0; i < TEST_DOUBLES.size() - 1; ++i) {
            double d1 = TEST_DOUBLES.get(i);
            double d2 = TEST_DOUBLES.get(i + 1);
            if (Double.isNaN(d1) || Double.isNaN(d2) ||
                Double.isInfinite(d1) || Double.isInfinite(d2)) {
                continue;
            }
            Rational r1 = new Rational(new BigDecimal(d1));
            Rational r2 = new Rational(new BigDecimal(d2));
            if (!Double.isInfinite(d1 * d2)) {
                assertDoubleTolerance(
                    d1 * d2, r1.multiply(r2).doubleValue());
            }
            if (d2 != 0 && !Double.isInfinite(d1 / d2)) {
                assertDoubleTolerance(d1 / d2, r1.divide(r2).doubleValue());
            }
            // We cannot generally test subtraction or addition. This is
            // because digits of accuracy can cancel out, making it
            // difficult to determine if the results are within tolerance of
            // each other. For example, if:
            // r1 = 1.00000000000000016
            // d1 = 1.0000000000000002
            // r2 = 1.00000000000000014
            // d2 = 1.0000000000000001
            // The d1-d2 will yield 1e-16 as the difference, but r1-r2 will
            // yield 2e-17. These appear to be very different results, but
            // are due to the limitations of precision with floating point
            // numbers and cancellation. Instead, we test addition of two
            // positive numbers or two negative numbers, and subtraction of
            // a negative from a positive or a positive from a negative.
            if ((d1 > 0) == (d2 > 0) && !Double.isInfinite(d1 + d2)) {
                assertDoubleTolerance(d1 + d2, r1.add(r2).doubleValue());
            } else if (!Double.isInfinite(d1 - d2)) {
                assertDoubleTolerance(d1 - d2, r1.subtract(r2).doubleValue());
            }
        }
    }

    /**
     * mod/floor mod rationals and verify the mod property that
     * (original - remainder) / mod is a whole number. Also verify that regular
     * mod result has the sign of the dividend, while floor mod result has
     * sign of the divisor.
     */
    @Test
    public void modsRationals() {
        for (int i = 0; i < TEST_RATIONALS.size() - 1; ++i) {
            Rational r = TEST_RATIONALS.get(i);
            Rational mod = TEST_RATIONALS.get(i + 1);
            if (mod.equals(Rational.ZERO)) {
                continue;
            }
            Rational result = r.subtract(r.mod(mod)).divide(mod);
            Assert.assertEquals(BigInteger.ONE, result.denominator());
            result = r.subtract(r.floorMod(mod)).divide(mod);
            Assert.assertEquals(BigInteger.ONE, result.denominator());

            if (r.equals(Rational.ZERO) ||
                r.divide(mod).denominator().equals(BigInteger.ONE)) {
                Assert.assertEquals(0, r.mod(mod).sign());
                Assert.assertEquals(0, r.floorMod(mod).sign());
            } else {
                Assert.assertEquals(r.sign(), r.mod(mod).sign());
                Assert.assertEquals(mod.sign(), r.floorMod(mod).sign());
            }
        }
    }

    @Test
    public void modsBigDecimals() {
        for (int i = 0;
             i < Math.min(TEST_RATIONALS.size(), TEST_BIG_DECIMALS.size());
             ++i) {
            Rational r = TEST_RATIONALS.get(i);
            BigDecimal mod = TEST_BIG_DECIMALS.get(i);
            if (mod.compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }
            Rational result = (r.subtract(r.mod(mod))).divide(mod);
            Assert.assertEquals(BigInteger.ONE, result.denominator());

            result = (r.subtract(r.floorMod(mod))).divide(mod);
            Assert.assertEquals(BigInteger.ONE, result.denominator());

            if (r.equals(Rational.ZERO) ||
                r.divide(mod).denominator().equals(BigInteger.ONE)) {
                Assert.assertEquals(0, r.mod(mod).sign());
                Assert.assertEquals(0, r.floorMod(mod).sign());
            } else {
                Assert.assertEquals(r.sign(), r.mod(mod).sign());
                Assert.assertEquals(mod.signum(), r.floorMod(mod).sign());
            }
        }
    }

    @Test
    public void modsBigIntegers() {
        for (int i = 0;
             i < Math.min(TEST_RATIONALS.size(), TEST_BIG_INTEGERS.size());
             ++i) {
            Rational r = TEST_RATIONALS.get(i);
            BigInteger mod = TEST_BIG_INTEGERS.get(i);
            if (mod.equals(BigInteger.ZERO)) {
                continue;
            }
            Rational result = r.subtract(r.mod(mod)).divide(mod);
            Assert.assertEquals(BigInteger.ONE, result.denominator());

            result = (r.subtract(r.floorMod(mod))).divide(mod);
            Assert.assertEquals(BigInteger.ONE, result.denominator());

            if (r.equals(Rational.ZERO) ||
                r.divide(mod).denominator().equals(BigInteger.ONE)) {
                Assert.assertEquals(0, r.mod(mod).sign());
                Assert.assertEquals(0, r.floorMod(mod).sign());
            } else {
                Assert.assertEquals(r.sign(), r.mod(mod).sign());
                Assert.assertEquals(mod.signum(), r.floorMod(mod).sign());
            }
        }
    }

    @Test
    public void modsLongs() {
        for (int i = 0;
             i < Math.min(TEST_RATIONALS.size(), TEST_LONGS.size());
             ++i) {
            Rational r = TEST_RATIONALS.get(i);
            long mod = TEST_LONGS.get(i);
            if (mod == 0) {
                continue;
            }
            Rational result = r.subtract(r.mod(mod)).divide(mod);
            Assert.assertEquals(BigInteger.ONE, result.denominator());

            result = r.subtract(r.floorMod(mod)).divide(mod);
            Assert.assertEquals(BigInteger.ONE, result.denominator());

            if (r.equals(Rational.ZERO) ||
                r.divide(mod).denominator().equals(BigInteger.ONE)) {
                Assert.assertEquals(0, r.mod(mod).sign());
                Assert.assertEquals(0, r.floorMod(mod).sign());
            } else {
                Assert.assertEquals(r.sign(), r.mod(mod).sign());
                if (mod < 0) {
                    Assert.assertEquals(-1, r.floorMod(mod).sign());
                } else {
                    Assert.assertEquals(1, r.floorMod(mod).sign());
                }
            }
        }
    }

    /**
     * For lists/arrays of rationals, sort them and compare the first and
     * last elements to the results from the min/max functions.
     */
    @Test
    public void minMax() {
        for (List<Rational> list: TEST_RATIONAL_LISTS) {
            if (list.isEmpty()) {
                continue;
            }
            List<Rational> copy = new ArrayList<>(list);
            Collections.sort(copy);
            Assert.assertEquals(copy.get(0), Rational.min(copy));
            Assert.assertEquals(copy.get(copy.size() - 1), Rational.max(copy));

            Rational[] arr = copy.toArray(new Rational[copy.size()]);
            Assert.assertEquals(copy.get(0), Rational.min(arr));
            Assert.assertEquals(copy.get(copy.size() - 1), Rational.max(arr));
        }
    }

    /**
     * For lists/arrays of rationals, sum them and compare the answer to
     * summing manually.
     */
    @Test
    public void sum() {
        for (List<Rational> list: TEST_RATIONAL_LISTS) {
            if (list.isEmpty()) {
                continue;
            }
            Rational[] arr = list.toArray(new Rational[list.size()]);
            Rational sum = Rational.ZERO;
            for (int i = 0; i < arr.length; ++i) {
                sum = sum.add(arr[i]);
            }
            Assert.assertEquals(sum, Rational.sum(list));
            Assert.assertEquals(sum, Rational.sum(arr));
        }

        for (int i = 0; i < TEST_RATIONALS.size() - 1; ++i) {
            Rational r1 = TEST_RATIONALS.get(i);
            Rational r2 = TEST_RATIONALS.get(i + 1);
            ArrayList<Rational> list = new ArrayList<>();
            list.add(r1);
            list.add(r2);
            Rational[] arr = list.toArray(new Rational[2]);
            Assert.assertEquals(r1.add(r2), Rational.sum(list));
            Assert.assertEquals(r1.add(r2), Rational.sum(arr));
        }

        for (int i = 0; i < TEST_RATIONALS.size() - 2; ++i) {
            ArrayList<Rational> list = new ArrayList<>();
            Rational r1 = TEST_RATIONALS.get(i);
            Rational r2 = TEST_RATIONALS.get(i + 1);
            Rational r3 = TEST_RATIONALS.get(i + 2);
            list.add(r1);
            list.add(r2);
            list.add(r3);
            Rational[] arr = list.toArray(new Rational[3]);
            Assert.assertEquals(r1.add(r2).add(r3), Rational.sum(list));
            Assert.assertEquals(r1.add(r2).add(r3), Rational.sum(arr));
        }
    }

    /**
     * For lists/arrays of rationals, multiply them and compare the answer to
     * the product.
     */
    @Test
    public void product() {
        for (List<Rational> list: TEST_RATIONAL_LISTS) {
            if (list.isEmpty()) {
                continue;
            }
            Rational[] arr = list.toArray(new Rational[list.size()]);
            Rational product = Rational.ONE;
            for (int i = 0; i < arr.length; ++i) {
                product = product.multiply(arr[i]);
            }
            Assert.assertEquals(product, Rational.product(list));
            Assert.assertEquals(product, Rational.product(arr));
        }

        for (int i = 0; i < TEST_RATIONALS.size() - 2; ++i) {
            Rational r1 = TEST_RATIONALS.get(i);
            Rational r2 = TEST_RATIONALS.get(i + 1);
            Rational r3 = TEST_RATIONALS.get(i + 2);

            List<Rational> list = new ArrayList<>();
            list.add(r1);
            list.add(r2);
            Rational[] arr = list.toArray(new Rational[2]);
            Assert.assertEquals(r1.multiply(r2), Rational.product(list));
            Assert.assertEquals(r1.multiply(r2), Rational.product(arr));

            list.add(r3);
            arr = list.toArray(new Rational[3]);
            Assert.assertEquals(r1.multiply(r2).multiply(r3),
                                Rational.product(list));
            Assert.assertEquals(r1.multiply(r2).multiply(r3),
                                Rational.product(arr));
        }
    }

    /**
     * Maximum relative tolerance for floating point calculations.
     */
    private static final float RELATIVE_EPSILON_F = 0.00001f;

    /**
     * Maximum absolute tolerance for floating point calculations.
     */
    private static final float ABSOLUTE_EPSILON_F = Float.MIN_NORMAL *
        (1 / RELATIVE_EPSILON_F);

    /**
     * Checks that the float values are within epsilon tolerance.
     * The check passes if the numbers are within either the relative tolerance
     * or the absolute tolerance.
     *
     * @param v1 the first value
     * @param v2 the second value
     */
    public static void assertFloatTolerance(float v1, float v2) {
        // If within the absolute tolerance, return.
        if (Math.abs(v1 - v2) < ABSOLUTE_EPSILON_F) {
            return;
        }
        // Otherwise, check the relative tolerance. First, find the larger of
        // the two, in absolute value.
        float larger = Math.max(Math.abs(v1), Math.abs(v2));
        // The absolute larger of the two cannot be zero -- otherwise, the
        // earlier absolute tolerance check would have returned.
        Assert.assertTrue(larger != 0.0f);
        // Check that the difference is within relative epsilon.
        Assert.assertEquals((v1 - v2) / larger, 0.0f, RELATIVE_EPSILON_F);
    }

    /**
     * Maximum relative tolerance for double precision floating point
     * calculations.
     */
    private static final double RELATIVE_EPSILON_D = 0.00000000000001;

    /**
     * Maximum absolute tolerance for double precision floating point
     * calculations.
     */
    private static final double ABSOLUTE_EPSILON_D = Double.MIN_NORMAL *
        (1 / RELATIVE_EPSILON_D);

    /**
     * Checks that the double values are within epsilon tolerance.
     * The check passes if the numbers are within either the relative tolerance
     * or the absolute tolerance.
     *
     * @param v1 the first value
     * @param v2 the second value
     */
    public static void assertDoubleTolerance(double v1, double v2) {
        // If within the absolute tolerance, return.
        if (Math.abs(v1 - v2) < ABSOLUTE_EPSILON_D) {
            return;
        }
        // Otherwise, check the relative tolerance. First, find the larger of
        // the two, in absolute value.
        double larger = Math.max(Math.abs(v1), Math.abs(v2));
        // The absolute larger of the two cannot be zero -- otherwise, the
        // earlier absolute tolerance check would have returned.
        Assert.assertTrue(larger != 0.0);
        // Check that the difference is within relative epsilon.
        Assert.assertEquals((v1 - v2) / larger, 0.0, RELATIVE_EPSILON_D);
    }
}
