/*
The MIT License (MIT)

Copyright (c) 2015 Elliott Karpilovsky

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package com.ekarp.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Arbitrary precision rational number. The sign of the number
 * is always associated with the numerator.
 */
public class Rational extends Number implements Comparable<Rational> {

    /**
     * Numerator.
     */
    private final BigInteger numer;

    /**
     * Denominator. Always positive.
     */
    private final BigInteger denom;

    /**
     * Representation of the number zero, in rational format.
     */
    public static Rational ZERO = new Rational(BigInteger.ZERO, BigInteger.ONE);

    /**
     * Representation of the number one, in rational format.
     */
    public static Rational ONE = new Rational(BigInteger.ONE, BigInteger.ONE);

    /**
     * Representation of the number 1/2, in rational format.
     */
    public static Rational ONE_HALF = new Rational(BigInteger.ONE,
                                                   BigInteger.valueOf(2));

    /**
     * Create a rational from two big integers.
     * @param numerator the numerator
     * @param denominator the denominator
     * @throws ArithmeticException if the denominator is zero
     */
    public Rational(BigInteger numerator, BigInteger denominator) {
        if (denominator.compareTo(BigInteger.ZERO) == 0) {
            throw new ArithmeticException(
                "Division by zero: " + numerator.toString() + "/0");
        }
        if (denominator.compareTo(BigInteger.ZERO) < 0) {
            numerator = numerator.negate();
        }
        // Remove any common factors between the numbers.
        BigInteger gcd = numerator.gcd(denominator);
        this.numer = numerator.divide(gcd);
        this.denom = denominator.divide(gcd).abs();
    }

    /**
     * Create a rational representing the big integer.
     * @param value the value
     */
    public Rational(BigInteger value) {
        this(value, BigInteger.ONE);
    }

    /**
     * Create a rational from a big decimal.
     *
     * @param bd the big decimal
     */
    public Rational(BigDecimal bd) {
        this(bigDecimalToNumerator(bd), bigDecimalToDenominator(bd));
    }

    /**
     * Create a rational from a string representing a decimal number or
     * "x/y" where x and y are base10 numbers.
     *
     * @param rational the number in string format; it can be in any
     *        format that is accepted by the BigDecimal constructor, or in the
     *        "x/y" format
     * @throws NumberFormatException if the string is not a valid representation
     *         of a decimal number or a rational number
     * @throws ArithmeticException if the denominator is zero
     */
    public Rational(String rational) {
        int numDivisors = 0;
        int lastOccur = 0;
        for (int i = 0; i < rational.length(); ++i) {
            if (rational.charAt(i) == '/') {
                lastOccur = i;
                numDivisors++;
                if (numDivisors > 1) {
                    break;
                }
            }
        }
        if (numDivisors == 0) {
            Rational r = new Rational(new BigDecimal(rational));
            this.numer = r.numerator();
            this.denom = r.denominator();
        } else if (numDivisors == 1) {
            String before = rational.substring(0, lastOccur);
            String after = rational.substring(lastOccur + 1, rational.length());
            Rational r = new Rational(new BigInteger(before),
                                      new BigInteger(after));
            this.numer = r.numerator();
            this.denom = r.denominator();
        } else {
            throw new NumberFormatException(
                "More than one divisor character: " + rational);
        }
    }

    /**
     * Create a rational from two longs.
     *
     * @param numerator the numerator
     * @param denominator the denominator
     * @throws ArithmeticException if the denominator is zero
     */
    public Rational(long numerator, long denominator) {
        this(BigInteger.valueOf(numerator), BigInteger.valueOf(denominator));
    }

    /**
     * Create a rational representing the long value.
     *
     * @param value the value
     */
    public Rational(long value) {
        this(BigInteger.valueOf(value), BigInteger.ONE);
    }

    /**
     * Converts a big decimal to the numerator part of a rational.
     *
     * @param bd the big decimal
     * @return the numerator part representing the big decimal
     */
    private static BigInteger bigDecimalToNumerator(BigDecimal bd) {
        if (bd.scale() <= 0) {
            return bd.toBigIntegerExact();
        } else {
            return bd.unscaledValue();
        }
    }

    /**
     * Converts a big decimal to the denominator part of a rational.
     *
     * @param bd the big decimal
     * @return the denominator part representing the big decimal
     */
    private static BigInteger bigDecimalToDenominator(BigDecimal bd) {
        if (bd.scale() <= 0) {
            return BigInteger.ONE;
        } else {
            return BigDecimal.ONE.scaleByPowerOfTen(bd.scale())
                .toBigIntegerExact();
        }
    }

    /**
     * Returns the numerator associated with the rational. The numerator always
     * carries the sign of the number.
     *
     * @return the numerator
     */
    public BigInteger numerator() {
        return numer;
    }

    /**
     * Returns the denominator associated with the rational. Guaranteed to be
     * a positive integer.
     *
     * @return the denominator
     */
    public BigInteger denominator() {
        return denom;
    }

    /**
     * Adds the rational to this one, returning a new rational.
     *
     * @param other the other rational number
     * @return the result from the addition
     */
    public Rational add(Rational other) {
        BigInteger n = numer.multiply(other.denom).add(
            other.numer.multiply(denom));
        BigInteger d = denom.multiply(other.denom);
        return new Rational(n, d);
    }

    /**
     * Adds the rational to the big integer, returning a new rational.
     *
     * @param other the other number
     * @return the result from the addition
     */
    public Rational add(BigInteger other) {
        return add(new Rational(other, BigInteger.ONE));
    }

    /**
     * Adds the rational to the big decimal, returning a new rational.
     *
     * @param other the other number
     * @return the result from the addition
     */
    public Rational add(BigDecimal other) {
        return add(new Rational(other));
    }

    /**
     * Adds the rational to the long value, returning a new rational.
     *
     * @param other the other number
     * @return the result from the addition
     */
    public Rational add(long other) {
        return add(new Rational(other, 1));
    }

    /**
     * Subtracts the rational from this one, returning a new rational.
     *
     * @param other the other rational number
     * @return the result from the subtraction
     */
    public Rational subtract(Rational other) {
        return add(other.negate());
    }

    /**
     * Subtracts the big integer from the rational, returning a new rational.
     *
     * @param other the other number
     * @return the result from the subtraction
     */
    public Rational subtract(BigInteger other) {
        return add(new Rational(other.negate(), BigInteger.ONE));
    }

    /**
     * Subtracts the big decimal from the rational, returning a new rational.
     *
     * @param other the other number
     * @return the result from the subtraction
     */
    public Rational subtract(BigDecimal other) {
        return add(new Rational(other).negate());
    }

    /**
     * Subtracts the long from the rational, returning a new rational.
     *
     * @param other the other number
     * @return the result from the subtraction
     */
    public Rational subtract(long other) {
        return add(new Rational(other, 1).negate());
    }

    /**
     * Multiplies the two rationals together, returning a new rational.
     *
     * @param other the other rational number
     * @return the result from the multiplication
     */
    public Rational multiply(Rational other) {
        BigInteger n = numer.multiply(other.numer);
        BigInteger d = denom.multiply(other.denom);
        return new Rational(n, d);
    }

    /**
     * Multiplies the rational with the big integer, returning a new rational.
     *
     * @param other the other number
     * @return the result from the multiplication
     */
    public Rational multiply(BigInteger other) {
        return multiply(new Rational(other, BigInteger.ONE));
    }

    /**
     * Multiplies the rational with the big decimal, returning a new rational.
     *
     * @param other the other number
     * @return the result from the multiplication
     */
    public Rational multiply(BigDecimal other) {
        return multiply(new Rational(other));
    }

    /**
     * Multiplies the rational with the long, returning a new rational.
     *
     * @param other the other number
     * @return the result from the multiplication
     */
    public Rational multiply(long other) {
        return multiply(new Rational(other, 1));
    }

    /**
     * Divides the two rationals, returning a new rational.
     *
     * @param other the other rational number
     * @return the result from the division
     * @throws ArithmeticException if other is zero
     */
    public Rational divide(Rational other) {
        return multiply(other.invert());
    }

    /**
     * Divides the rational by the big integer, returning a new rational.
     *
     * @param other the other number
     * @return the result from the division
     * @throws ArithmeticException if other is zero
     */
    public Rational divide(BigInteger other) {
        return divide(new Rational(other, BigInteger.ONE));
    }

    /**
     * Divides the rational by the big decimal, returning a new rational.
     *
     * @param other the other number
     * @return the result from the division
     * @throws ArithmeticException if other is zero
     */
    public Rational divide(BigDecimal other) {
        return divide(new Rational(other));
    }

    /**
     * Divides the rational by the long, returning a new rational.
     *
     * @param other the other number
     * @return the result from the division
     * @throws ArithmeticException if other is zero
     */
    public Rational divide(long other) {
        return divide(new Rational(other, 1));
    }

    /**
     * Returns the absolute value of this rational.
     *
     * @return the absolute value of the rational
     */
    public Rational abs() {
        return new Rational(numer.abs(), denom);
    }

    /**
     * Returns a rational that is the negated version of this one.
     *
     * @return a negated rational of the same absolute value
     */
    public Rational negate() {
        return new Rational(numer.negate(), denom);
    }

    /**
     * Returns the reciprocal of the rational.
     *
     * @return the rational representing (denominator / numerator)
     * @throws ArithmeticException if this rational is zero
     */
    public Rational invert() {
        return new Rational(denom, numer);
    }

    /**
     * Returns the floor of the rational (largest integer less than or equal
     * to it).
     *
     * @return the big integer representing the floor of the rational
     */
    public BigInteger floor() {
        return round(RoundingMode.FLOOR);
    }

    /**
     * Returns the ceiling of the rational (smallest integer greater than or
     * equal to it).
     *
     * @return the big integer representing the ceiling of the rational
     */
    public BigInteger ceiling() {
        return round(RoundingMode.CEILING);
    }

    /**
     * Round the rational to the nearest integer (using the HALF_EVEN
     * rounding mode).
     *
     * @return the big integer representing the rounded rational
     * @see RoundingMode#HALF_EVEN
     */
    public BigInteger round() {
        return round(RoundingMode.HALF_EVEN);
    }

    /**
     * Round the rational to the nearest integer (using the specified
     * rounding mode).
     *
     * @param roundingMode the rounding mode to use
     * @return the big integer representing the rounded rational
     * @throws ArithmeticException if the rounding mode is UNNECESSARY
     *         but conversion to a big integer requires rounding
     * @throws IllegalArgumentException if roundingMode is not one of
     *         CEILING, DOWN, FLOOR, HALF_DOWN, HALF_EVEN, HALF_UP,
     *         UNNECESSARY, or UP
     */
    public BigInteger round(RoundingMode roundingMode) {
        BigInteger[] result = numer.divideAndRemainder(denom);
        if (result[1].equals(BigInteger.ZERO)) {
            return result[0];
        }
        Rational absRemainder = new Rational(result[1].abs(), denom);
        // Use if/else instead of switch/case, as switching on
        // an enum causes generation of an inner class (which splits
        // the class file in two). Since there are only 6 cases to handle,
        // the performance difference is negligible.
        if (roundingMode.equals(RoundingMode.HALF_EVEN)) {
            int c = absRemainder.compareTo(ONE_HALF);
            if (c > 0) {
                return roundUp(result[0], sign());
            } else if (c < 0) {
                return result[0];
            } else {
                BigInteger m = result[0].mod(BigInteger.valueOf(2));
                if (m.equals(BigInteger.ZERO)) {
                    return result[0];
                } else {
                    return roundUp(result[0], sign());
                }
            }
        } else if (roundingMode.equals(RoundingMode.CEILING)) {
            if (numer.signum() == -1) {
                return result[0];
            }
            return result[0].add(BigInteger.ONE);
        } else if (roundingMode.equals(RoundingMode.FLOOR)) {
            if (numer.signum() == 1) {
                return result[0];
            }
            return result[0].subtract(BigInteger.ONE);
        } else if (roundingMode.equals(RoundingMode.HALF_DOWN)) {
            int c = absRemainder.compareTo(ONE_HALF);
            if (c > 0) {
                return roundUp(result[0], sign());
            } else {
                return result[0];
            }
        } else if (roundingMode.equals(RoundingMode.DOWN)) {
            return result[0];
        } else if (roundingMode.equals(RoundingMode.HALF_UP)) {
            int c = absRemainder.compareTo(ONE_HALF);
            if (c < 0) {
                return result[0];
            }
            return roundUp(result[0], sign());
        } else if (roundingMode.equals(RoundingMode.UP)) {
            return roundUp(result[0], sign());
        } else if (roundingMode.equals(RoundingMode.UNNECESSARY)) {
            throw new ArithmeticException(
                "Rounding mode is UNNECESSARY, but remainder exists");
        } else {
            throw new IllegalArgumentException(
                "Unsupported rounding mode: " + roundingMode.toString());
        }
    }

    /**
     * Rounds the big integer up to the next integer.
     *
     * @param bi the big integer
     * @param sign the original sign of the number
     * @return the result of rounding the integer up (result is always larger
     *         in magnitude)
     */
    private static BigInteger roundUp(BigInteger bi, int sign) {
        if (sign > 0) {
            return bi.add(BigInteger.ONE);
        } else {
            return bi.subtract(BigInteger.ONE);
        }
    }

    /**
     * Returns this value modded by the given rational. The sign
     * of returned result is the same sign as the modulo value
     * (following Java's pattern for the Math.floorMod function).
     *
     * @param moduloValue the modulo value
     * @return the value of this rational, modulo the given value
     * @throws ArithmeticException if the moduloValue is zero
     */
    public Rational floorMod(Rational moduloValue) {
        Rational divided = divide(moduloValue);
        BigInteger newNumer = divided.round(RoundingMode.FLOOR);
        return subtract(moduloValue.multiply(newNumer));
    }

    /**
     * Returns this value modded by the given big integer. The sign
     * of returned result is the same sign as the modulo value
     * (following Java's pattern for the Math.floorMod function).
     *
     * @param moduloValue the modulo value
     * @return the value of this rational, modulo the given value
     * @throws ArithmeticException if the moduloValue is zero
     */
    public Rational floorMod(BigInteger moduloValue) {
        return floorMod(new Rational(moduloValue, BigInteger.ONE));
    }

    /**
     * Returns this value modded by the given big decimal. The sign
     * of returned result is the same sign as the modulo value
     * (following Java's pattern for the Math.floorMod function).
     *
     * @param moduloValue the modulo value
     * @return the value of this rational, modulo the given value
     * @throws ArithmeticException if the moduloValue is zero
     */
    public Rational floorMod(BigDecimal moduloValue) {
        return floorMod(new Rational(moduloValue));
    }

    /**
     * Returns this value modded by the given long. The sign
     * of returned result is the same sign as the modulo value
     * (following Java's pattern for the Math.floorMod function).
     *
     * @param moduloValue the modulo value
     * @return the value of this rational, modulo the given value
     * @throws ArithmeticException if the moduloValue is zero
     */
    public Rational floorMod(long moduloValue) {
        return floorMod(new Rational(moduloValue, 1));
    }

    /**
     * Returns this value modded by the given rational. The sign
     * of returned result is the same sign as this rational number
     * (following Java's pattern for the % operator).
     *
     * @param m the modulo value
     * @return the value of this rational, modulo the given value
     * @throws ArithmeticException if the moduloValue is zero
     */
    public Rational mod(Rational m) {
        BigInteger q = divide(m).round(RoundingMode.DOWN);
        return subtract(m.multiply(q));
    }

    /**
     * Returns this value modded by the given big integer. The sign
     * of returned result is the same sign as this rational number
     * (following Java's pattern for the % operator).
     *
     * @param moduloValue the modulo value
     * @return the value of this rational, modulo the given value
     * @throws ArithmeticException if the moduloValue is zero
     */
    public Rational mod(BigInteger moduloValue) {
        return mod(new Rational(moduloValue, BigInteger.ONE));
    }

    /**
     * Returns this value modded by the given big decimal. The sign
     * of returned result is the same sign as this rational number
     * (following Java's pattern for the % operator).
     *
     * @param moduloValue the modulo value
     * @return the value of this rational, modulo the given value
     * @throws ArithmeticException if the moduloValue is zero
     */
    public Rational mod(BigDecimal moduloValue) {
        return mod(new Rational(moduloValue));
    }

    /**
     * Returns this value modded by the given long. The sign
     * of returned result is the same sign as this rational number
     * (following Java's pattern for the % operator).
     *
     * @param moduloValue the modulo value
     * @return the value of this rational, modulo the given value
     * @throws ArithmeticException if the moduloValue is zero
     */
    public Rational mod(long moduloValue) {
        return mod(new Rational(moduloValue, 1));
    }

    /**
     * Raise this rational to the N-th power.
     *
     * @param n the power to raise the rational to
     * @return the rational raised to the N-th power
     */
    public Rational pow(int n) {
        if (n >= 0) {
            return new Rational(numer.pow(n), denom.pow(n));
        } else if (n == Integer.MIN_VALUE) {
            Rational i = invert();
            return i.pow(Integer.MAX_VALUE).multiply(i);
        } else {
            return invert().pow(-1 * n);
        }
    }

    /**
     * Returns the sign of this rational.
     *
     * @return the sign of the rational (-1 if negative, 0 if zero, 1 if
     *         positive)
     */
    public int sign() {
        return numer.signum();
    }

    /**
     * Takes the minimum of the rationals.
     *
     * @param rationals an arbitrary number of rationals
     * @return the minimum of the rationals
     * @throws IllegalArgumentException if no rationals are passed in
     */
    public static Rational min(Rational... rationals) {
        return min(Arrays.asList(rationals));
    }

    /**
     * Takes the minimum of the rationals.
     *
     * @param rationals an iterable of rationals (e.g., a list)
     * @return the minimum of the rationals
     * @throws IllegalArgumentException if no rationals are passed in
     */
    public static Rational min(Iterable<Rational> rationals) {
        Iterator<Rational> iter = rationals.iterator();
        if (!iter.hasNext()) {
            throw new IllegalArgumentException(
                "Must pass in at least one rational to minimum");
        }
        Rational smallest = iter.next();
        while (iter.hasNext()) {
            Rational next = iter.next();
            if (smallest.compareTo(next) > 0) {
                smallest = next;
            }
        }
        return smallest;
    }

    /**
     * Takes the maximum of the rationals.
     *
     * @param rationals an arbitrary number of rationals
     * @return the maximum of the rationals
     * @throws IllegalArgumentException if no rationals are passed in
     */
    public static Rational max(Rational... rationals) {
        return max(Arrays.asList(rationals));
    }

    /**
     * Takes the maximum of the rationals.
     *
     * @param rationals an iterable of rationals (e.g., a list)
     * @return the maximum of the rationals
     * @throws IllegalArgumentException if no rationals are passed in
     */
    public static Rational max(Iterable<Rational> rationals) {
        Iterator<Rational> iter = rationals.iterator();
        if (!iter.hasNext()) {
            throw new IllegalArgumentException(
                "Must pass in at least one rational to maximum");
        }
        Rational largest = iter.next();
        while (iter.hasNext()) {
            Rational next = iter.next();
            if (largest.compareTo(next) < 0) {
                largest = next;
            }
        }
        return largest;
    }

    /**
     * Convenience method to sum an arbitrary number of rationals.
     *
     * @param rationals an arbitrary number of rationals
     * @return the sum of the rationals
     * @throws IllegalArgumentException if no rationals are passed in
     */
    public static Rational sum(Rational... rationals) {
        return sum(Arrays.asList(rationals));
    }

    /**
     * Convenience method to sum an iterable of rationals.
     *
     * @param rationals an iterable of rationals (e.g., a list)
     * @return the sum of the rationals
     * @throws IllegalArgumentException if no rationals are passed in
     */
    public static Rational sum(Iterable<Rational> rationals) {
        Iterator<Rational> iter = rationals.iterator();
        if (!iter.hasNext()) {
            throw new IllegalArgumentException(
                "Must pass in at least one rational to sum");
        }
        Rational sum = iter.next();
        while (iter.hasNext()) {
            sum = sum.add(iter.next());
        }
        return sum;
    }

    /**
     * Convenience method to multiply an arbitrary number of rationals.
     *
     * @param rationals an arbitrary number of rationals
     * @return the product of the rationals
     * @throws IllegalArgumentException if no rationals are passed in
     */
    public static Rational product(Rational... rationals) {
        return product(Arrays.asList(rationals));
    }

    /**
     * Convenience method to multiply an arbitrary number of rationals.
     *
     * @param rationals an iterable of rationals (e.g., a list)
     * @return the product of the rationals
     * @throws IllegalArgumentException if no rationals are passed in
     */
    public static Rational product(Iterable<Rational> rationals) {
        Iterator<Rational> iter = rationals.iterator();
        if (!iter.hasNext()) {
            throw new IllegalArgumentException(
                "Must pass in at least one rational to product");
        }
        Rational product = iter.next();
        while (iter.hasNext()) {
            product = product.multiply(iter.next());
        }
        return product;
    }

    /**
     * Converts this rational to a big decimal, throwing an exception
     * if the division results in a non-terminating decimal expansion.
     *
     * @return the rational as a BigDecimal
     * @throws ArithmeticException if the quotient does not have a terminating
     *         decimal expansion
     */
    public BigDecimal bigDecimalValue() {
        return new BigDecimal(numer).divide(new BigDecimal(denom));
    }

    /**
     * Converts this rational to a big decimal, using the math context for
     * rounding.
     *
     * @param mc the math context to use for rounding
     * @return the rational as a BigDecimal, rounding according to the math
     *         context
     * @throws ArithmeticException if the quotient does not have a terminating
     *         decimal expansion and the math context has either 1.) a precision
     *          of 0 (meaning exact precision) or 2.) a rounding mode of
     *          UNNECESSARY
     */
    public BigDecimal bigDecimalValue(MathContext mc) {
        return new BigDecimal(numer).divide(
            new BigDecimal(denom), mc);
    }

    /**
     * Returns the rational as a byte (rounding down). If the result is too
     * big to fit in a byte, only the low-order 8 bits are returned. May
     * result in loss of precision, magnitude, and even wrong sign.
     *
     * @return the rational as a byte
     */
    @Override
    public byte byteValue() {
        return new BigDecimal(numer).divideToIntegralValue(
            new BigDecimal(denom)).byteValue();
    }

    /**
     * The number of significant decimal digits that can be represented in a
     * double.
     */
    private static final int DOUBLE_SIGFIG = 17;

    /**
     * Returns the rational as a double. Does so by converting it to a
     * big decimal with sufficiently high precision and calling BigDecimal's
     * doubleValue() method. Note that:
     * <ol>
     *  <li>if the number has a magnitude greater than what double can hold,
     *  positive or negative infinity will be used (depending on the sign)
     *  </li>
     *  <li>the conversion may lose precision</li>
     *  </li>
     * </ol>
     * </p>
     *
     * @return the rational as a double
     * @see BigDecimal#doubleValue
     */
    @Override
    public double doubleValue() {
        return bigDecimalValue(
            new MathContext(calcPrecision(DOUBLE_SIGFIG),
                            RoundingMode.DOWN)).doubleValue();
    }

    /**
     * The number of significant decimal digits that can be represented in a
     * float.
     */
    private static final int FLOAT_SIGFIG = 9;

    /**
     * Returns the rational as a float. Does so by converting it to a
     * big decimal with sufficiently high precision and calling BigDecimal's
     * floatValue() method. Note that:
     * <ol>
     *  <li>if the number has a magnitude greater than what float can hold,
     *  positive or negative infinity will be used (depending on the sign)
     *  </li>
     *  <li>the conversion may lose precision</li>
     *  </li>
     * </ol>
     *
     * @return the rational as a float
     * @see BigDecimal#floatValue
     * @see #doubleValue
     */
    @Override
    public float floatValue() {
        return bigDecimalValue(
            new MathContext(calcPrecision(FLOAT_SIGFIG),
                            RoundingMode.DOWN)).floatValue();
    }

    /**
     * Approximately Math.log(2) / Math.log(10), rounded up slightly. Ensures
     * that ((int) (bits * UB_BITS_TO_DEC_DIGITS) + 1) is never an
     * underestimation of the number of decimal digits.
     */
    private static final double UB_BITS_TO_DEC_DIGITS = 0.301029995664;

    /**
     * Approximately Math.log(2) / Math.log(10), rounded down slightly. Ensures
     * that ((int) (bits * LB_BITS_TO_DEC_DIGITS)) is never an overestimation
     * of the number of decimal digits.
     */
    private static final double LB_BITS_TO_DEC_DIGITS = 0.301029995663;

    /**
     * <p>Calculate the precision needed for conversion to a floating point
     * number. Uses the formula:
     * (max(numSigFig + 1, max(1, 1 + (upper bound on number of digits in
     * numerator) - (lower bound on number of digits in denominator))) + (1 +
     * upper bound on number of decimal digits in numerator)).</p>
     * <p>The formula for precision is derived from the fact that we need
     * numSigFig decimal digits of precision. However, because there may need
     * to be rounding (and we do not want to make assumptions about BigDecimal's
     * rounding methods, as they may change in the future), we need to include
     * additional digits. We need the next digit to inform BigDecimal's method
     * if the last digit digit should be rounded. So we actually need
     * numSigFig + 1. However, if the extra digit is a 5, we need more digits
     * so BigDecimal can determine if it should round up or down. In particular,
     * we need enough digits so we are guaranteed a non-zero digit occurs after
     * the extra digit.</p>
     * <p>When computing division on the remainder, a non-zero digit will
     * occur after (1 + number of decimal digits in the denominator); the
     * remainder is multiplied by 10 each time a zero is generated, a non-zero
     * digit must be generated after that many times (since then the remainder
     * will be larger than the divisor).</p>
     * <p>To get to the point where division is being computed on the remainder,
     * we need to output all the digits before the decimal point. This is
     * bounded above by max(1, 1 + (number of digits in numerator) - (number of
     * digits in denominator)). However, these digits overlap with the
     * numSigFig + 1 digits. Since we need numSigFig + 1 digits already, we
     * just need the max between numSigFig + 1 and the bound mentioned.</p>
     * <p>Calculating the exact number of decimal digits needed is more
     * intensive than calculating the upper/lower bounds to within
     * +/- (0.00000001% + 2), so bounds are used instead.</p>
     *
     * @param numSigFig number of significant figures needed for the conversion
     * @return the precision needed so BigDecimal's conversions methods can
     * unambiguously convert the rational into a floating point number with
     * the desired number of significant figures
     */
    private int calcPrecision(int numSigFig) {
        int numDenomBits = denom.bitLength();
        int upperBoundNumerBits = numer.bitLength();
        // These calculations may slightly over-estimate the number of digits,
        // which is fine.
        int upperBoundNumerDigits = (
            (int) (upperBoundNumerBits * UB_BITS_TO_DEC_DIGITS)) + 1;
        // If the numerator is negative, add an extra digit, as the bitLength()
        // method does not include the sign bit on negative numbers and can
        // generate a smaller than expected value, e.g.,
        // new BigInteger("-128").bitLength() == 7. We pad it by an extra digit
        // for simplicity (incrementing numDenomBits can result in overflow that
        // needs to be handled) and correctness.
        if (numer.signum() == -1) {
            upperBoundNumerDigits++;
        }
        int upperBoundDenomDigits = (
            (int) (numDenomBits * UB_BITS_TO_DEC_DIGITS)) + 1;
        // This may slightly under-estimate the number of digits, which is fine.
        int lowerBoundDenomDigits =
            (int) (numDenomBits * LB_BITS_TO_DEC_DIGITS);
        int digitsToRemainder = Math.max(
            1, 1 + upperBoundNumerDigits - lowerBoundDenomDigits);
        return Math.max(numSigFig + 1, digitsToRemainder) +
            1 + upperBoundDenomDigits;
    }

    /**
     * Returns the rational as an integer (rounding down). If the result is too
     * big to fit in an int, only the low-order 32 bits are returned. May
     * result in loss of precision, magnitude, and even wrong sign.
     *
     * @return the rational as an integer
     */
    @Override
    public int intValue() {
        return new BigDecimal(numer).divideToIntegralValue(
            new BigDecimal(denom)).intValue();
    }

    /**
     * Returns the rational as a long (rounding down). If the result is too
     * big to fit in a long, only the low-order 64 bits are returned. May
     * result in loss of precision, magnitude, and even wrong sign.
     *
     * @return the rational as a long
     */
    @Override
    public long longValue() {
        return new BigDecimal(numer).divideToIntegralValue(
            new BigDecimal(denom)).longValue();
    }

    /**
     * Returns the rational as a short (rounding down). If the result is too
     * big to fit in a short, only the low-order 16 bits are returned. May
     * result in loss of precision, magnitude, and even wrong sign.
     *
     * @return the rational as a short
     */
    @Override
    public short shortValue() {
        return new BigDecimal(numer).divideToIntegralValue(
            new BigDecimal(denom)).shortValue();
    }

    /**
     * Computes the hash code of the rational.
     *
     * @return the hash code
     */
    @Override
    public int hashCode() {
        // 83 and 5 are arbitrary prime numbers to make the hash more unique.
        return 83 * (5 + numer.hashCode()) + denom.hashCode();
    }

    /**
     * Performs an equality check with the other object.
     *
     * @return true if the two objects are equal, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Rational)) {
            return false;
        }
        return compareTo((Rational) obj) == 0;
    }

    /**
     * Compares this rational with the other one.
     *
     * @param other the other rational
     * @return a negative integer, zero, or a positive integer if this
     *         rational is less than, equal to, or greater than other
     */
    @Override
    public int compareTo(Rational other) {
        return subtract(other).numerator().compareTo(BigInteger.ZERO);
    }

    /**
     * Convert this rational into a string. Zero is represented as "0".
     * Any number with a denominator of 1 is represented by just the
     * numerator.
     *
     * @return the rational as a string
     */
    @Override
    public String toString() {
        if (numer.equals(BigInteger.ZERO)) {
            return "0";
        }
        if (denom.equals(BigInteger.ONE)) {
            return numer.toString();
        }
        return numer.toString() + "/" + denom.toString();
    }
}
